package il.co.techschool.workermanagement.activites;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import il.co.techschool.workermanagement.R;
import il.co.techschool.workermanagement.adapters.AdapterWorkTimeList;
import il.co.techschool.workermanagement.common.AppWorkerPresence;

/**
 * Created by Admin on 10/14/2017.
 */

public class ActWorkTimeList extends AppCompatActivity {

    private TextView mtxtWrkTimeWorkingDay;
    private TextView mtxtWrkTimeEntranceWork;
    private TextView mtxtWrkTimeExitWork;
    private TextView mtxtWrkTimeWorkerProject;
    private TextView mtxtWrkTimeWorkerTotalHours;
    private ListView mListViewWorkTime;

    private Context mContext;
    private AppWorkerPresence mAppWorkerPresence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        mAppWorkerPresence = (AppWorkerPresence) getApplication();
        initComponents();
    }

    @Override
    protected void onResume() {
        super.onResume();

        AdapterWorkTimeList adapterWorkTimeList;
        adapterWorkTimeList = new AdapterWorkTimeList(mContext, mAppWorkerPresence.getArrayListWorkTimeList());
        mListViewWorkTime.setAdapter(adapterWorkTimeList);
    }

    private void initComponents() {
        setContentView(R.layout.act_work_time_list);

        mtxtWrkTimeWorkingDay = (TextView) findViewById(R.id.txtWrkTimeWorkingDay);
        mtxtWrkTimeEntranceWork = (TextView) findViewById(R.id.txtWrkTimeEntranceWork);
        mtxtWrkTimeExitWork = (TextView) findViewById(R.id.txtWrkTimeExitWork);
        mtxtWrkTimeWorkerProject = (TextView) findViewById(R.id.txtWrkTimeWorkerProject);
        mtxtWrkTimeWorkerTotalHours = (TextView) findViewById(R.id.txtWrkTimeWorkerTotalHours);
        mListViewWorkTime = (ListView) findViewById(R.id.lstWorkTimeLst);
    }
}
