package il.co.techschool.workermanagement.arrays;


import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Date;

import il.co.techschool.workermanagement.entities.WorkerLocation;
import il.co.techschool.workermanagement.events.EventArrayListWorkerLocationsChange;

/**
 * Created by Admin on 11/24/2017.
 */

public class ArrayListLocation extends ArrayList<WorkerLocation> {
    private Date loaded;

    public Date getLoaded() {
        return loaded;
    }

    public void setLoaded(Date loaded) {
        this.loaded = loaded;
    }

    public WorkerLocation findWorkerLocationById(String aUid) {
        WorkerLocation workerLocationFound;
        int iIndex;
        int iSize;

        workerLocationFound = null;

        iSize = size();

        for (iIndex = 0; iIndex < iSize; iIndex++) {


            if (aUid.compareToIgnoreCase(get(iIndex).getDbId()) == 0) {
                workerLocationFound = get(iIndex);
                break;
            }
        }

        return workerLocationFound;
    }

    public WorkerLocation updateWorkerLocation(WorkerLocation aWorkerLocation) {
        WorkerLocation currentWorkerLocation;

        currentWorkerLocation = findWorkerLocationById(aWorkerLocation.getDbId());

        if (currentWorkerLocation != null) {
            currentWorkerLocation.updateFrom(aWorkerLocation);
            EventBus.getDefault().post(new EventArrayListWorkerLocationsChange());
            return currentWorkerLocation;
        } else {
            return null;
        }
    }

    public void removeWorkerLocation(WorkerLocation aWorkerLocation) {
        WorkerLocation currentWorkerLocation;

        currentWorkerLocation = findWorkerLocationById(aWorkerLocation.getDbId());

        if (currentWorkerLocation != null) {
            remove(currentWorkerLocation);
            EventBus.getDefault().post(new EventArrayListWorkerLocationsChange());
        }
    }

    @Override
    public boolean add(WorkerLocation aWorkerLocation) {
        boolean result;
        result = super.add(aWorkerLocation);
        EventBus.getDefault().post(new EventArrayListWorkerLocationsChange());
        return result;
    }
}
