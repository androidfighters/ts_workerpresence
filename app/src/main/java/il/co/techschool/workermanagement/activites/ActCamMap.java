package il.co.techschool.workermanagement.activites;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import il.co.techschool.workermanagement.R;


public class ActCamMap extends AppCompatActivity {

    private Button mBtnClick;
    private Button mbtnShowMap;
    private ImageView imageView;

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    Context context = null;

    // directory name to store captured images
    private static final String IMAGE_DIRECTORY_NAME = "Asisoft";
    private Uri fileUri; // file url to store image
    //  GPSTracker gps;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cam_map);

        context = getApplicationContext();

        initializeComponents();

        initializeComponentsListener();

    }

    void initializeComponents() {
        mBtnClick = (Button) findViewById(R.id.mBtnClick);
        mbtnShowMap = (Button) findViewById(R.id.btnShowMap);
        imageView = (ImageView) findViewById(R.id.imageView);

    }

    void initializeComponentsListener() {
        mbtnShowMap.setOnClickListener(OnClick);
        mBtnClick.setOnClickListener(OnClick);
    }

    View.OnClickListener OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()) {

                // Capture image button click event
                case R.id.mBtnClick:
                    ActivityCompat.requestPermissions(ActCamMap.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);

                    // capture picture
                    captureImage();

                    //  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    //   startActivityForResult(intent,CAM_REQUEST);
                    //   File file = getFile();
                    //    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

                    GPSTracker gt = new GPSTracker(getApplicationContext());
                    break;
                case R.id.btnShowMap:

                    Intent showmap = new Intent(ActCamMap.this, ActMapsActivity.class);
                    startActivity(showmap);
                    break;
            }
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {

//return image View
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(bitmap);

            if (resultCode == RESULT_OK) {
                previewCapturedImage();

                //  gps
                GPSTracker gt = new GPSTracker(getApplicationContext());
                Location l = gt.getLocation();
                if (l == null) {
                    Toast.makeText(getApplicationContext(), "GPS Unable !! CHECK NETWORK", Toast.LENGTH_LONG).show();
                } else {
                    double lat = l.getLatitude();
                    double lon = l.getLongitude();
                    Toast.makeText(getApplicationContext(), " Lat = " + lat + "\n lon = " + lon, Toast.LENGTH_LONG).show();
                }


            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "Cancelled", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Error!", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    private void captureImage() {


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


        ////  fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);////

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
// start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }


    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }
    // public static Uri getOutputMediaFileUri(File file) {
    //     return Uri.fromFile(file);
    //  }


    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {

                Log.d(IMAGE_DIRECTORY_NAME, "Fisierul "
                        + IMAGE_DIRECTORY_NAME + " nu a fost creat");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFileName;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFileName = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFileName;
    }


    private void previewCapturedImage() {
        try {
            imageView.setVisibility(View.VISIBLE);

            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                    options);

            imageView.setImageBitmap(bitmap);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // new GPS Informations
            // get it by gps.getLatitude() for example
        }
    };
}