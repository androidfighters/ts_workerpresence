package il.co.techschool.workermanagement.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Admin on 11/16/2017.
 */

public class WorkerLocation {

    @SerializedName("db_id")
    @Expose
    private String dbid;
    @SerializedName("workerID")
    @Expose
    private Integer workerID;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    private Context mContext;

    public WorkerLocation() {

    }

    public WorkerLocation(String aDbID, int aWorkerID, String aLongitude, String aLatitude) {


        dbid = aDbID;
        workerID = aWorkerID;
        longitude = aLongitude;
        latitude = aLatitude;
    }

    public String getDbId() {
        return dbid;
    }

    public void setDbId(String aDbId) {
        this.dbid = aDbId;
    }

    public int getWorkerID() {
        return workerID;
    }

    public void setWorkerID(int aWorkerID) {
        this.workerID = aWorkerID;
    }

    public void setLongitude(String aLongitude) {
        this.longitude = aLongitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLatitude(String aLatitude) {
        this.latitude = aLatitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void updateFrom(WorkerLocation aWorkerLocation) {
        dbid = aWorkerLocation.dbid;
        workerID = aWorkerLocation.workerID;
        longitude = aWorkerLocation.longitude;
        latitude = aWorkerLocation.latitude;


    }
}
