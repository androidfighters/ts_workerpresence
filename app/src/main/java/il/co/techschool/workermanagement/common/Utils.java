package il.co.techschool.workermanagement.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Admin on 10/15/2017.
 */

public final class Utils {
    private Utils() {

    }

    final static String mdbDateFormat = "dd/MM/yyyy";
    final static String mdbDateTimeFormat = "dd/MM/yyyy HH:mm:ss.sss";

    public static Date StringToDate(String aDate) {

        SimpleDateFormat format = new SimpleDateFormat(mdbDateFormat);
        try {
            Date date = format.parse(aDate);
            return date;
        } catch (ParseException e) {
            return null;
        }
    }

    public static String DateToString(Date aDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(mdbDateFormat);
        try {

            String datetime = dateFormat.format(aDate);
            return datetime;
        } catch (Exception e) {
            // System.out.print(e.getStackTrace().toString());
            return "";
        }

    }

    public static Date StringToDateTime(String aDateTime) {
        if (aDateTime == "")
            return null;

        SimpleDateFormat format = new SimpleDateFormat(mdbDateTimeFormat);
        try {
            Date datetime = format.parse(aDateTime);
            return datetime;
        } catch (ParseException e) {
            return null;
        }
    }

    public static String DateTimeToString(Date aDateTime) {
        if (aDateTime == null)
            return "";


        SimpleDateFormat dateFormat = new SimpleDateFormat(mdbDateTimeFormat);
        try {

            String datetime = dateFormat.format(aDateTime);
            return datetime;
        } catch (Exception e) {
            // System.out.print(e.getStackTrace().toString());
            return "";
        }
    }


}
