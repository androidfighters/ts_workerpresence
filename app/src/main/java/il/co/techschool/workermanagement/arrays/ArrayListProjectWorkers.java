package il.co.techschool.workermanagement.arrays;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Date;

import il.co.techschool.workermanagement.entities.ProjectWorkers;
import il.co.techschool.workermanagement.events.EventArrayListProjectWorkersChange;

/**
 * Created by Admin on 12/12/2017.
 */

public class ArrayListProjectWorkers extends ArrayList<ProjectWorkers> {
    private Date loaded;

    public Date getLoaded() {
        return loaded;
    }

    public void setLoaded(Date loaded) {
        this.loaded = loaded;
    }

    public ProjectWorkers findProjectWorkersById(String aUid) {
        ProjectWorkers projectWorkersFound;
        int iIndex;
        int iSize;

        projectWorkersFound = null;

        iSize = size();

        for (iIndex = 0; iIndex < iSize; iIndex++) {
            if (aUid.compareToIgnoreCase(String.valueOf(get(iIndex).getDbId())) == 0) {
                projectWorkersFound = get(iIndex);
                break;
            }

        }

        return projectWorkersFound;
    }

    public ProjectWorkers updateProjectWorkers(ProjectWorkers aProjectWorkers) {
        ProjectWorkers currentProjectWorkers;

        currentProjectWorkers = findProjectWorkersById(String.valueOf(aProjectWorkers.getDbId()));

        if (currentProjectWorkers != null) {
            currentProjectWorkers.updateFrom(aProjectWorkers);
            EventBus.getDefault().post(new EventArrayListProjectWorkersChange());
            return currentProjectWorkers;
        } else {
            return null;
        }
    }

    public void removeProjectWorkers(ProjectWorkers aProjectWorkers) {
        ProjectWorkers currentProjectWorkers;

        currentProjectWorkers = findProjectWorkersById(String.valueOf(aProjectWorkers.getDbId()));

        if (currentProjectWorkers != null) {
            remove(currentProjectWorkers);
            EventBus.getDefault().post(new EventArrayListProjectWorkersChange());
        }
    }

    @Override
    public boolean add(ProjectWorkers aProjectWorkers) {
        boolean result;
        result = super.add(aProjectWorkers);
        EventBus.getDefault().post(new EventArrayListProjectWorkersChange());
        return result;
    }
}
