package il.co.techschool.workermanagement.arrays;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Date;

import il.co.techschool.workermanagement.entities.WorkTimeList;
import il.co.techschool.workermanagement.events.EventArrayListWorkTimeListChange;

/**
 * Created by Admin on 10/16/2017.
 */

public class ArrayListWorkTimeList extends ArrayList<WorkTimeList> {
    private Date loaded;

    public Date getLoaded() {
        return loaded;
    }

    public void setLoaded(Date loaded) {
        this.loaded = loaded;
    }

    public WorkTimeList findWorkTimeListById(String aUid) {
        WorkTimeList workTimeListFound;
        int iIndex;
        int iSize;

        workTimeListFound = null;

        iSize = size();

        for (iIndex = 0; iIndex < iSize; iIndex++) {
            if (aUid.compareToIgnoreCase(get(iIndex).getDbId()) == 0) {
                workTimeListFound = get(iIndex);
                break;
            }
        }

        return workTimeListFound;
    }

    public WorkTimeList updateWorkTimeList(WorkTimeList aWorkTimeList) {
        WorkTimeList currentWorkTimeList;

        currentWorkTimeList = findWorkTimeListById(aWorkTimeList.getDbId());

        if (currentWorkTimeList != null) {
            currentWorkTimeList.updateFrom(aWorkTimeList);
            EventBus.getDefault().post(new EventArrayListWorkTimeListChange());
            return currentWorkTimeList;
        } else {
            return null;
        }
    }

    public void removeWorkTimeList(WorkTimeList aWorkTimeList) {
        WorkTimeList currentWorkTimeList;

        currentWorkTimeList = findWorkTimeListById(aWorkTimeList.getDbId());

        if (currentWorkTimeList != null) {
            remove(currentWorkTimeList);
            EventBus.getDefault().post(new EventArrayListWorkTimeListChange());
        }
    }

    @Override
    public boolean add(WorkTimeList aWorkTimeList) {
        boolean result;
        result = super.add(aWorkTimeList);
        EventBus.getDefault().post(new EventArrayListWorkTimeListChange());
        return result;
    }
}
