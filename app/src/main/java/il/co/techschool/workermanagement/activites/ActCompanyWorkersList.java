package il.co.techschool.workermanagement.activites;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import il.co.techschool.workermanagement.R;
import il.co.techschool.workermanagement.adapters.AdapterWorkers;
import il.co.techschool.workermanagement.common.AppWorkerPresence;

/**
 * Created by Admin on 1/2/2018.
 */

public class ActCompanyWorkersList extends AppCompatActivity {
    private ListView mlstWrkListListView;

    private Context mContext;
    private AppWorkerPresence mAppWorkerPresence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_company_workers_list);

        mAppWorkerPresence = (AppWorkerPresence) getApplication();
        mContext = this;

        initComponents();
    }

    @Override
    protected void onResume() {
        super.onResume();

        AdapterWorkers adapterWorkers;
        // start using the adapter after you have the data loading...
        adapterWorkers = new AdapterWorkers(mContext, AppWorkerPresence.APP_INSTANCE.getArrayListWorkers());
        mlstWrkListListView.setAdapter(adapterWorkers);
    }
    private void initComponents() {
        mlstWrkListListView = (ListView) findViewById(R.id.lstProjectWorkers);
    }
}
