package il.co.techschool.workermanagement.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 12/11/2017.
 */

public class Company {
    @SerializedName("db_id")
    @Expose
    private String dbId;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("owner_id")
    @Expose
    private Integer ownerId;
    @SerializedName("location_id")
    @Expose
    private Integer locationId;
    @SerializedName("company_icon")
    @Expose
    private byte[] companyIcon;
    private Context mContext;

    public Company() {

    }

    public Company(String aDbId, String aCompanyName, Integer aOwnerId, Integer aLocationId, byte[] aCompanyIcon) {
        dbId = aDbId;
        companyName = aCompanyName;
        ownerId = aOwnerId;
        locationId = aLocationId;
        companyIcon =aCompanyIcon;
    }

    public String getDbId() {
        return dbId;
    }

    public void setDbId(String aDbId) {
        dbId = aDbId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String aCompanyName) {
        companyName = aCompanyName;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer aOwnerId) {
        ownerId = aOwnerId;
    }

    public Integer getLocationId() {
        return locationId;
    }
    public void setLocationId(Integer aLocationId) {
        locationId = aLocationId;
    }

    public byte[] getCompanyIcon()
    {
        return companyIcon;
    }

    public void setCompanyIcon(byte[] aCompanyIcon)
    {
        this.companyIcon = aCompanyIcon;
    }

    public void updateFrom(Company aCompany) {
        dbId = aCompany.dbId;
        companyName = aCompany.companyName;
        ownerId = aCompany.ownerId;
        locationId = aCompany.locationId;
        companyIcon = aCompany.companyIcon;
    }

}
