package il.co.techschool.workermanagement.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import il.co.techschool.workermanagement.R;
import il.co.techschool.workermanagement.activites.ActCompany;
import il.co.techschool.workermanagement.activites.ActProject;
import il.co.techschool.workermanagement.arrays.ArrayListProjects;
import il.co.techschool.workermanagement.common.AppWorkerPresence;
import il.co.techschool.workermanagement.entities.Project;
import il.co.techschool.workermanagement.entities.ProjectWorkers;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by Admin on 11/26/2017.
 */

public class AdapterProjects extends BaseAdapter
{

    private ActProject mActProject;
    private Context mContext;
    private ArrayListProjects mArrayListProjects;

    public AdapterProjects(Context aContext, ArrayListProjects aArrayListProjects)
    {
        mArrayListProjects = aArrayListProjects;
        mContext = aContext;
    }

    @Override
    public int getCount()
    {
        return mArrayListProjects.size();
    }

    @Override
    public Object getItem(int i)
    {
        return mArrayListProjects.getLoaded();
    }

    @Override
    public long getItemId(int i)
    {
        return 0;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent)
    {
        View resultView;
        final LayoutInflater layoutInflater;
        final Project currentProject;

        TextView txtProjectName;
        TextView txtProjectStatus;
        ImageView imgProjectIcon;
        ImageButton imgBtnProject;

        if (convertView == null)
        {
            layoutInflater = (LayoutInflater) AppWorkerPresence.APP_INSTANCE.getSystemService(LAYOUT_INFLATER_SERVICE);
            // create the custom layout to show the "workers"
            resultView = layoutInflater.inflate(R.layout.lo_item_project, null);
            // find the objects inside the custom layout...
            txtProjectName = (TextView) resultView.findViewById((R.id.txtItmProjectName));
            txtProjectStatus = (TextView) resultView.findViewById((R.id.txtItmProjectStatus));
            imgProjectIcon = (ImageView) resultView.findViewById((R.id.imgItmProjectIcon));
            imgBtnProject = (ImageButton) resultView.findViewById((R.id.imgBtnItmProject));

            resultView.setTag(R.id.txtItmProjectName, txtProjectName);
            resultView.setTag(R.id.txtItmProjectStatus, txtProjectStatus);
            resultView.setTag(R.id.imgItmProjectIcon, imgProjectIcon);
            resultView.setTag(R.id.imgBtnItmProject, imgBtnProject);

        } else
        {
            resultView = convertView;
            // extract all the pointers to the objects from the "tag" list.
            txtProjectName = (TextView) resultView.getTag(R.id.txtItmProjectName);
            txtProjectStatus = (TextView) resultView.getTag(R.id.txtItmProjectStatus);
            imgProjectIcon = (ImageView) resultView.getTag(R.id.imgItmProjectIcon);
            imgBtnProject = (ImageButton) resultView.getTag(R.id.imgBtnItmProject);
        }
        currentProject = mArrayListProjects.get(position);

        // update custom items in the view with the data from the "currentWorker";
        txtProjectName.setText(currentProject.getProjectName());
        txtProjectStatus.setText(String.valueOf(currentProject.getProjectStatus()));

        byte[] icon = currentProject.getProjectIcon();
        if (icon != null)
        {
            Bitmap bitmap = BitmapFactory.decodeByteArray(icon, 0, icon.length);
            imgProjectIcon.setImageBitmap(bitmap);
        }
        txtProjectName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, " get click", Toast.LENGTH_SHORT).show();

                Bundle dataBundle = new Bundle();
                dataBundle.putInt("id", currentProject.getDbId());
                Intent intent = new Intent(mContext, ActProject.class);
                intent.putExtras(dataBundle);
                mContext.startActivity(intent);

            }
        });
        try {
            imgBtnProject.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    switch (v.getId()) {
                        case R.id.imgBtnItmProject:
                            PopupMenu popup = new PopupMenu(mContext, v);
                            popup.getMenuInflater().inflate(R.menu.popup_menu_act_projects_list,
                                    popup.getMenu());
                            popup.show();
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                            {

                                @Override
                                public boolean onMenuItemClick(MenuItem item)
                                {

                                    switch (item.getItemId()) {

                                        case R.id.ItmProjectEdit:

                                            Bundle dataBundle = new Bundle();
                                            dataBundle.putInt("id", currentProject.getDbId());
                                            Intent intent = new Intent(mContext, ActProject.class);
                                            intent.putExtras(dataBundle);
                                            mContext.startActivity(intent);
                                            return true;

                                        case R.id.ItmAddIcon:

                                            break;
                                        case R.id.ItmProjectAddWorker:

                                            int projectID = currentProject.getDbId();
                                            addWorkerAlertDialog(projectID);
                                            break;
                                        case R.id.ItmProjectRemoveWorker:

                                            int currentProjectDbId = currentProject.getDbId();
                                            removeWorkerAlertDialog(currentProjectDbId);
                                            break;
                                        default:
                                            break;
                                    }
                                    return true;
                                }
                            });
                            break;
                        default:
                            break;
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultView;
    }

    private void addWorkerAlertDialog(final int aProjectID) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.act_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText metDialogWorkerID = (EditText) dialogView.findViewById(R.id.etDialogWorkerID);
        final EditText metDialogSalary = (EditText) dialogView.findViewById(R.id.etDialogSalary);

        dialogBuilder.setTitle("Custom dialog");
        dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {

                ProjectWorkers projectWorkers = new ProjectWorkers();

                String workerPhone = metDialogWorkerID.getText().toString();
                String salary = metDialogSalary.getText().toString();

                if (!workerPhone.isEmpty()) {
                    String sWorkerId = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerID(workerPhone);

                    int projectID = aProjectID;
                    int workerID = Integer.parseInt(sWorkerId);
                    int Salary = Integer.parseInt(salary);

                    projectWorkers.setProjectID(projectID);
                    projectWorkers.setWorkerID(workerID);
                    projectWorkers.setStatus(1);
                    projectWorkers.setSalary(Salary);

                    AppWorkerPresence.APP_INSTANCE.getWMDBAPI().addWorkersToProject(projectWorkers);
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void removeWorkerAlertDialog(final int aProjectID) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.act_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText metDialogWorkerID = (EditText) dialogView.findViewById(R.id.etDialogWorkerID);
        final EditText metDialogSalary = (EditText) dialogView.findViewById(R.id.etDialogSalary);


        dialogBuilder.setTitle("Custom dialog");
        dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {

                ProjectWorkers projectWorkers = new ProjectWorkers();

                String workerPhone = metDialogWorkerID.getText().toString();
                String salary = metDialogSalary.getText().toString();

                if (!workerPhone.isEmpty()) {
                    String sWorkerId = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerID(workerPhone);

                    int projectID = aProjectID;
                    int workerID = Integer.parseInt(sWorkerId);
                    int Salary = Integer.parseInt(salary);

                    projectWorkers.setProjectID(projectID);
                    projectWorkers.setWorkerID(workerID);
                    projectWorkers.setStatus(1);
                    projectWorkers.setSalary(Salary);

                    AppWorkerPresence.APP_INSTANCE.getWMDBAPI().removeWorkerFromProject(workerID, projectID);
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }
}