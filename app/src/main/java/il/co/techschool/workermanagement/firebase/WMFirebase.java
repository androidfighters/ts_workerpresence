package il.co.techschool.workermanagement.firebase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import il.co.techschool.workermanagement.common.AppWorkerPresence;
import il.co.techschool.workermanagement.entities.Project;
import il.co.techschool.workermanagement.entities.WorkTimeList;
import il.co.techschool.workermanagement.entities.Worker;

/**
 * Created by Admin on 10/25/2017.
 */

public class WMFirebase {
    private static final String TAG = "WMFirebase";
    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBReferenceWorkers;
    private DatabaseReference mDBReferenceWorkTimeList;
    private DatabaseReference mDBReferenceProjects;
    private DatabaseReference mDBReferenceCompanies;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser mCurrentUser;

    private static final WMFirebase
            ourInstance = new WMFirebase();

    public static WMFirebase getInstance() {
        return ourInstance;
    }

    private WMFirebase() {
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    mCurrentUser = user;
                    opendatabase();

                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }

        };

        mAuth.addAuthStateListener(mAuthListener);
    }

    private void opendatabase() {
        mDatabase = FirebaseDatabase.getInstance();

        AppWorkerPresence.APP_INSTANCE.getArrayListWorkers();

        AppWorkerPresence.APP_INSTANCE.getArrayListWorkTimeList();

        AppWorkerPresence.APP_INSTANCE.getArrayListProjects();

        AppWorkerPresence.APP_INSTANCE.getArrayListCompanies();

        mDBReferenceWorkers = mDatabase.getReference("workers");
        mDBReferenceWorkers.addValueEventListener(OnValueWorker);
        mDBReferenceWorkers.addChildEventListener(OnChildWorker);

        mDBReferenceWorkTimeList = mDatabase.getReference("work_time_list");
        mDBReferenceWorkTimeList.addValueEventListener(OnValueWorkTimeList);
        mDBReferenceWorkTimeList.addChildEventListener(OnChildWorkTimeList);

        mDBReferenceProjects = mDatabase.getReference("projects");
        mDBReferenceProjects.addValueEventListener(OnValueProject);
        mDBReferenceProjects.addChildEventListener(OnChildProject);

        mDBReferenceCompanies = mDatabase.getReference("companies");
        //  mDBReferenceCompanies.addValueEventListener(OnValueCompany);
        // mDBReferenceCompanies.addChildEventListener(OnChildCompany);

    }

    public FirebaseUser getCurrentUser() {
        return mCurrentUser;
    }

    private ValueEventListener OnValueWorker = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot aDataSnapshot) {
            Log.d(TAG, "OnValueWorker:onDataChange");
        }

        @Override
        public void onCancelled(DatabaseError aDatabaseError) {
            Log.d(TAG, "OnValueWorker:onCancelled");
        }
    };

    private ValueEventListener OnValueProject = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Log.d(TAG, "OnValueProject:onDataChange");
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.d(TAG, "OnValueProject:onDataCancelled");
        }
    };


    private ValueEventListener OnValueWorkTimeList = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot aDataSnapshot) {
            Log.d(TAG, "OnValueWorkTimeList:onDataChange");
        }

        @Override
        public void onCancelled(DatabaseError aDatabaseError) {
            Log.d(TAG, "OnValueWorkTimeList:onCancelled");
        }
    };
    private ChildEventListener OnChildWorker = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildWorker:onChildAdded");
            Worker currentWorker;

            currentWorker = aDataSnapshot.getValue(Worker.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListWorkers().add(currentWorker);
        }


        @Override
        public void onChildChanged(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildWorker:onChildChanged");
            Worker currentWorker;

            currentWorker = aDataSnapshot.getValue(Worker.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListWorkers().updateWorker(currentWorker);
        }

        @Override
        public void onChildRemoved(DataSnapshot aDataSnapshot) {
            Log.d(TAG, "OnChildWorker:onChildRemoved");

            Worker currentWorker;

            currentWorker = aDataSnapshot.getValue(Worker.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListWorkers().removeWorker(currentWorker);
        }

        @Override
        public void onChildMoved(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildWorker:onChildMoved");
        }

        @Override
        public void onCancelled(DatabaseError aDatabaseError) {
            Log.d(TAG, "OnChildWorker:onCancelled");
        }
    };

    private ChildEventListener OnChildProject = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildProject:onChildAdded");
            Project currentProject;
            currentProject = aDataSnapshot.getValue(Project.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListProjects().add(currentProject);
        }

        @Override
        public void onChildChanged(DataSnapshot aDataSnapshot, String s) {
            Log.d(TAG, "OnChildProject:onChildChanged");
            Project currentProject;

            currentProject = aDataSnapshot.getValue(Project.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListProjects().updateProject(currentProject);
        }

        @Override
        public void onChildRemoved(DataSnapshot aDataSnapshot) {
            Log.d(TAG, "OnChildProject:onChildRemoved");

            Project currentProject;

            currentProject = aDataSnapshot.getValue(Project.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListProjects().removeProject(currentProject);
        }

        @Override
        public void onChildMoved(DataSnapshot aDataSnapShot, String s) {
            Log.d(TAG, "OnChildProject:onChildMoved");
        }

        @Override
        public void onCancelled(DatabaseError aDatabaseError) {
            Log.d(TAG, "OnChildProject:onCancelled");
        }
    };
    private ChildEventListener OnChildWorkTimeList = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildWorkTimeList:onChildAdded");
            WorkTimeList currentWorkTimeList;

            currentWorkTimeList = aDataSnapshot.getValue(WorkTimeList.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListWorkTimeList().add(currentWorkTimeList);
        }

        @Override
        public void onChildChanged(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildWorkTimeList:onChildChanged");
            WorkTimeList currentWorkTimeList;

            currentWorkTimeList = aDataSnapshot.getValue(WorkTimeList.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListWorkTimeList().updateWorkTimeList(currentWorkTimeList);
        }

        @Override
        public void onChildRemoved(DataSnapshot aDataSnapshot) {
            Log.d(TAG, "OnChildWorkTimeList:onChildRemoved");

            WorkTimeList currentWorkTimeList;

            currentWorkTimeList = aDataSnapshot.getValue(WorkTimeList.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListWorkTimeList().removeWorkTimeList(currentWorkTimeList);
        }

        @Override
        public void onChildMoved(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildWorkTimeList:onChildMoved");
        }

        @Override
        public void onCancelled(DatabaseError aDatabaseError) {
            Log.d(TAG, "OnChildWorkTimeList:onCancelled");
        }
    };


    public void updateWorkersStatus(String aWorkerUUID, Integer aStatusNew) {
        mDBReferenceWorkers.child(aWorkerUUID).child("status").setValue(aStatusNew);
    }

    public void addWorker(Worker worker) {
        mDBReferenceWorkers.child(worker.getDbId()).setValue(worker);
    }


    public void addWorkTimeList(WorkTimeList mCurrentTimeList) {
        mDBReferenceWorkTimeList.child(mCurrentTimeList.getDbId()).setValue(mCurrentTimeList);
    }

    public void addProject(Project project) {
        mDBReferenceProjects.child(String.valueOf(project.getDbId())).setValue(project);
    }
}
