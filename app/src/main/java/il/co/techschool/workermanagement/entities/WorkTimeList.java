package il.co.techschool.workermanagement.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Admin on 10/16/2017.
 */

public class WorkTimeList {
    @SerializedName("db_id")
    @Expose
    private String dbid;
    @SerializedName("workerID")
    @Expose
    private Integer workerID;
    @SerializedName("entrance_work")
    @Expose
    private Date entranceWork;
    @SerializedName("exit_work")
    @Expose
    private Date exitWork;
    @SerializedName("enter_locationID")
    @Expose
    private Integer enterLocationID;
    @SerializedName("exit_locationID")
    @Expose
    private Integer exitLocationID;
    @SerializedName("projectID")
    @Expose
    private Integer projectID;
    @SerializedName("companyID")
    @Expose
    private Integer companyID;

    public WorkTimeList() {
    }

    public WorkTimeList(Integer aWorkerID, Date aEntranceWork, Date aExitWork,
                        String aDbID, Integer aEnterLocationID, Integer aExitLocationID, Integer aProjectID, Integer aCompanyID) {

        workerID = aWorkerID;
        entranceWork = aEntranceWork;
        exitWork = aExitWork;
        dbid = aDbID;
        enterLocationID = aEnterLocationID;
        exitLocationID = aExitLocationID;
        projectID = aProjectID;
        companyID = aCompanyID;
    }

    public String getDbId() {
        return dbid;
    }

    public void setDbId(String aDbId) {
        this.dbid = aDbId;
    }

    public int getWorkerID() {
        return workerID;
    }

    public void setWorkerID(int aWorkerID) {
        this.workerID = aWorkerID;
    }

    public void setEntranceWork(Date aEntranceWork) {
        this.entranceWork = aEntranceWork;
    }

    public Date getEntranceWork() {
        return exitWork;
    }

    public Date getExitWork() {
        return exitWork;
    }

    public void setExitWork(Date aExitWork) {
        this.exitWork = aExitWork;
    }

    public int getEnterLocationID() {
        return enterLocationID;
    }

    public void setEnterLocationID(int aEnterLocationID) {
        this.enterLocationID = aEnterLocationID;
    }

    public int getExitLocationID() {
        return exitLocationID;
    }

    public void setExitLocationID(int aExitLocationID) {
        this.exitLocationID = aExitLocationID;
    }

    public int getProjectID() {
        return projectID;
    }

    public void setProjectID(Integer aProjectID) {
        this.projectID = aProjectID;
    }

    public int getCompanyID() {
        return companyID;
    }

    public void setCompanyID(Integer aCompanyID) {
        this.companyID = aCompanyID;
    }

    public void updateFrom(WorkTimeList aWorkTimeList) {
        dbid = aWorkTimeList.dbid;
        workerID = aWorkTimeList.workerID;
        entranceWork = aWorkTimeList.entranceWork;
        exitWork = aWorkTimeList.exitWork;
        enterLocationID = aWorkTimeList.enterLocationID;
        exitLocationID = aWorkTimeList.exitLocationID;
        projectID = aWorkTimeList.projectID;
        companyID = aWorkTimeList.companyID;

    }
}
