package il.co.techschool.workermanagement.activites;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;

import il.co.techschool.workermanagement.R;
import il.co.techschool.workermanagement.common.AppWorkerPresence;
import il.co.techschool.workermanagement.common.Utils;
import il.co.techschool.workermanagement.entities.Project;

/**
 * Created by Admin on 11/25/2017.
 */

public class ActProject extends AppCompatActivity {
    private TextView mtxtProjectIcon;
    private TextView mtxtNewProjectTitle;
    private TextView mtxtNewProjectProjectName;
    private TextView mtxtNewProjectManagerID;
    private TextView mtxtNewProjectLocation;
    private TextView mtxtNewProjectDescription;
    private TextView mtxtNewProjectStatus;
    private TextView mtxtNewProjectStartDate;
    private TextView mtxtNewProjectEndDate;

    private EditText metNewProjectProjectName;
    private EditText metNewProjectManagerID;
    private EditText metNewProjectLocation;
    private EditText metNewProjectDescription;
    private EditText metNewProjectStatus;
    private EditText metNewProjectStartDate;
    private EditText metNewProjectEndDate;

    private Button mbtnProjectWorkTime;
    private Button mbtnProjectWorkersList;
    private Button mbtnNewProjectSave;
    private Button mbtnProjectIcon;

    private ImageView mimgProjectIcon;
    public Bundle getBundle = null;

    private Project mProject;

    private Context mContext;

    private static final int PICK_IMAGE = 100;
    final int REQUEST_CODE_GALLERY = 999;

    Uri mImgUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_project);

        mContext = this;

        initializeComponents();
        initializeComponentsListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // check if there is a project to download its details or start new one
        getBundle = this.getIntent().getExtras();
        if (getBundle != null) {
            if (getBundle.containsKey("id")) {
                loadProjectDetails(getBundle.getInt("id"));

            }
        }
    }

    void initializeComponents() {
        mtxtNewProjectTitle = (TextView) findViewById(R.id.txtNewProjectTitle);
        metNewProjectProjectName = (EditText) findViewById(R.id.etNewProjectProjectName);
        metNewProjectManagerID = (EditText) findViewById(R.id.etNewProjectManagerID);
        metNewProjectLocation = (EditText) findViewById(R.id.etNewProjectLocation);
        metNewProjectDescription = (EditText) findViewById(R.id.etNewProjectDescription);
        metNewProjectStatus = (EditText) findViewById(R.id.etNewProjectStatus);
        metNewProjectStartDate = (EditText) findViewById(R.id.etNewProjectStartDate);
        metNewProjectEndDate = (EditText) findViewById(R.id.etNewProjectEndDate);

        mbtnProjectWorkTime = (Button) findViewById(R.id.btnProjectWorkTime);
        mbtnProjectWorkersList = (Button) findViewById(R.id.btnProjectWorkersList);
        mbtnNewProjectSave = (Button) findViewById(R.id.btnNewProjectSave);
        mbtnProjectIcon = (Button) findViewById(R.id.btnProjectIcon);

        mimgProjectIcon = (ImageView) findViewById(R.id.imgProjectIcon);

    }

    void initializeComponentsListeners() {
        mbtnProjectWorkTime.setOnClickListener(OnClick);
        mbtnProjectWorkersList.setOnClickListener(OnClick);
        mbtnNewProjectSave.setOnClickListener(OnClick);
        mbtnProjectIcon.setOnClickListener(OnClick);
    }

    View.OnClickListener OnClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.btnNewProjectSave:

                    // save project details
                    saveProjectDetails();

                    break;
                case R.id.btnProjectWorkersList:

                    if (mProject != null) {
                        // get projectID
                        String ProjectID = String.valueOf(mProject.getDbId());
                        // set projectID
                        AppWorkerPresence.APP_INSTANCE.setProjectID(ProjectID);
                        // load project workers in a list
                        Intent ActWorkersList = new Intent(ActProject.this, ActProjectWorkersList.class);
                        startActivity(ActWorkersList);

                    } else {
                        // show message
                        Toast errorToast = Toast.makeText(ActProject.this, " There is no workers for this project yet ! ", Toast.LENGTH_SHORT);
                        errorToast.show();
                    }

                    break;

                case R.id.btnProjectWorkTime:
                    // go to work time act
                    Intent ActWorkTime = new Intent(ActProject.this, ActWorkTime.class);
                    startActivity(ActWorkTime);
                    break;

                case R.id.btnProjectIcon:
                    ActivityCompat.requestPermissions(
                            ActProject.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_CODE_GALLERY
                    );
                    break;
            }
        }
    };

    public static byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CODE_GALLERY){
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK ,
                MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                intent.setType("image/*");
                // try cut image
//                intent.putExtra("crop", "true");
//                intent.putExtra("aspectX", 1);
//                intent.putExtra("aspectY", 1);
//                intent.putExtra("outputX", 200);
//                intent.putExtra("outputY", 200);
//                intent.putExtra("return-data", true);
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            }
            else {
                Toast.makeText(getApplicationContext(), "You don't have permission to access file location!", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null){
            mImgUri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(mImgUri);

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                mimgProjectIcon.setImageBitmap(bitmap);
                //mimgProjectIcon.setImageURI(mImgUri);
                //mimgProjectIcon.setImageBitmap((Bitmap) data.getExtras().get("data"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /*
        save and update project
         */
    public void saveProjectDetails() {
        // check if id != null as update project and if not create a new project
        if (mProject == null) {
            mProject = new Project();
        }

        // get written details
        String projectName = metNewProjectProjectName.getText().toString();
        String tmpManagerPhone = metNewProjectManagerID.getText().toString();
        String tmpLocation = metNewProjectLocation.getText().toString();
        String description = metNewProjectDescription.getText().toString();
        String tmpStatus = metNewProjectStatus.getText().toString();
        String tmpstartdate = metNewProjectStartDate.getText().toString();
        String tmpendDate = metNewProjectEndDate.getText().toString();

        if (projectName.isEmpty() || tmpManagerPhone.isEmpty() || tmpLocation.isEmpty() || description.isEmpty() ||
                tmpStatus.isEmpty() || tmpstartdate.isEmpty()) {
            // show message
            Toast errorToast = Toast.makeText(ActProject.this, " Fields are empty! ", Toast.LENGTH_SHORT);
            errorToast.show();

        } else {

            // get owner id by his phone number
            String tmpManagerID = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerID(tmpManagerPhone);
            if (tmpManagerID == null) {
                // show message
                Toast errorToast = Toast.makeText(ActProject.this, " Manager phone number isn't correct ", Toast.LENGTH_SHORT);
                errorToast.show();
            } else {
                // compare string to integer
                int managerID = Integer.parseInt(tmpManagerID);

                int status = Integer.parseInt(tmpStatus);
                int location = Integer.parseInt(tmpLocation);

                // compare string to date
                Date startDate = Utils.StringToDate(tmpstartdate);


                // save details
                mProject.setProjectName(projectName);
                mProject.setManagerId(managerID);
                mProject.setLocationId(location);
                mProject.setDescription(description);
                mProject.setProjectStatus(status);
                mProject.setStartDate(startDate);
                if (!tmpendDate.isEmpty()) {
                    Date endDate = Utils.StringToDate(tmpendDate);
                    mProject.setEndDate(endDate);
                }

                mimgProjectIcon.setImageResource(R.mipmap.ic_launcher);

                if (mimgProjectIcon != null)
                {
                    mProject.setProjectIcon(imageViewToByte(mimgProjectIcon));
                }
                AppWorkerPresence.APP_INSTANCE.getWMDBAPI().saveProject(mProject);

                // show message mProject saved
                Toast errorToast = Toast.makeText(ActProject.this, " Project saved successfully! ", Toast.LENGTH_SHORT);
                errorToast.show();

                // intent back to  list
                Intent actWorkersList = new Intent(ActProject.this, ActMain.class);
                startActivity(actWorkersList);
            }
        }
    }


    /*
    load project details
     */
    public void loadProjectDetails(Integer aProjectID) {
        mProject = AppWorkerPresence.APP_INSTANCE.getArrayListProjects().findProjectById(aProjectID);
        metNewProjectProjectName.setText(mProject.getProjectName());
        metNewProjectManagerID.setText(String.valueOf(mProject.getManagerId()));
        metNewProjectLocation.setText(String.valueOf(mProject.getLocationId()));
        metNewProjectDescription.setText(mProject.getDescription());
        metNewProjectStatus.setText(String.valueOf(mProject.getProjectStatus()));

        String startDate = Utils.DateToString(mProject.getStartDate());
        String endDate = Utils.DateToString(mProject.getEndDate());

        metNewProjectStartDate.setText(startDate);
        metNewProjectEndDate.setText(endDate);

    }
}
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 100 && resultCode == RESULT_OK) {
//            // imageUri = data.getData();
//            // mimgProjectIcon.setImageURI(imageUri);
//
//            mimgProjectIcon.setImageBitmap((Bitmap) data.getExtras().get("data"));
//        }
//    }
//    private void openGallery() {
//        Intent gallery = new Intent(Intent.ACTION_PICK,
//                MediaStore.Images.Media.INTERNAL_CONTENT_URI);
//        gallery.putExtra("crop", "true");
//        gallery.putExtra("aspectX", 1);
//        gallery.putExtra("aspectY", 1);
//        gallery.putExtra("outputX", 200);
//        gallery.putExtra("outputY", 200);
//        gallery.putExtra("return-data", true);
//
//        startActivityForResult(gallery, PICK_IMAGE);
//    }