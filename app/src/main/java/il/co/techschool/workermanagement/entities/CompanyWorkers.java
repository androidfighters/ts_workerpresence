package il.co.techschool.workermanagement.entities;

import com.google.android.gms.common.api.Status;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import il.co.techschool.workermanagement.common.WorkerStatus;

/**
 * Created by Admin on 12/11/2017.
 */

public class CompanyWorkers {
    @SerializedName("db_id")
    @Expose
    private String dbId;
    @SerializedName("company_id")
    @Expose
    private Integer companyId;
    @SerializedName("worker_Id")
    @Expose
    private Integer workerId;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("salary")
    @Expose
    private Integer salary;

    public CompanyWorkers() {

    }

    public CompanyWorkers(String aDbId, Integer aCompanyId, Integer aWorkerId, Integer aStatus, Integer aSalary) {
        dbId = aDbId;
        companyId = aCompanyId;
        workerId = aWorkerId;
        status = aStatus;
        salary = aSalary;

    }

    public String getDbId() {
        return dbId;
    }

    public void setDbId(String aDbId) {
        dbId = aDbId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer aCompanyId) {
        companyId = aCompanyId;
    }

    public Integer getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Integer aWorkerId) {
        workerId = aWorkerId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer aStatus) {
        status = aStatus;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer aSalary) {
        salary = aSalary;
    }

    public void updateFrom(CompanyWorkers aCompanyWorkers) {
        dbId = aCompanyWorkers.dbId;
        companyId = aCompanyWorkers.companyId;
        workerId = aCompanyWorkers.workerId;
        status = aCompanyWorkers.status;
        salary = aCompanyWorkers.salary;

    }

}
