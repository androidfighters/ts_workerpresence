package il.co.techschool.workermanagement.activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import il.co.techschool.workermanagement.R;
import il.co.techschool.workermanagement.common.AppWorkerPresence;
import il.co.techschool.workermanagement.common.Utils;
import il.co.techschool.workermanagement.entities.Worker;

import static il.co.techschool.workermanagement.activites.ActLogin.PhoneNumber;

/**
 * Created by Admin on 10/14/2017.
 */

public class ActWorkerDetails extends AppCompatActivity {
    private Context mContext;
    private TextView mtxtWrkDetailsTitle;
    private TextView mtxtWrkDetailsFirstName;
    private TextView mtxtWrkDetailsLastName;
    private TextView mtxtWrkDetailsIsraeliID;
    private TextView mtxtWrkDetailsDOB;
    private EditText metWrkDetailsFirstName;
    private EditText metWrkDetailsLastName;
    private EditText metWrkDetailsIsraeliID;
    private EditText metWrkDetailsDOB;
    private Button mbtnWrkDetailsSaveProperties;

    public Bundle getBundle = null;

    private Worker mWorker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_worker_details);

        mContext = this;

        initComponents();
        iniitComponentsListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // check if we got id to load the user details
        getBundle = this.getIntent().getExtras();
        if (getBundle != null) {
            if (getBundle.containsKey("ID")) {
                String id = getBundle.getString("ID");

                if (id != null) {
                    loadWorkerDetails(id);
                }
            }
        }
    }

    private void initComponents() {
        mtxtWrkDetailsTitle = (TextView) findViewById(R.id.txtWrkDetailsTitle);
        mtxtWrkDetailsFirstName = (TextView) findViewById(R.id.txtWrkDetailsFirstName);
        mtxtWrkDetailsLastName = (TextView) findViewById(R.id.txtWrkDetailsLastName);
        mtxtWrkDetailsIsraeliID = (TextView) findViewById(R.id.txtWrkDetailsIsraeliID);
        mtxtWrkDetailsDOB = (TextView) findViewById(R.id.txtWrkDetailsDOB);

        metWrkDetailsFirstName = (EditText) findViewById(R.id.etWrkDetailsFirstName);
        metWrkDetailsLastName = (EditText) findViewById(R.id.etWrkDetailsLastName);
        metWrkDetailsIsraeliID = (EditText) findViewById(R.id.etWrkDetailsIsraeliID);
        metWrkDetailsDOB = (EditText) findViewById(R.id.etWrkDetailsDOB);
        mbtnWrkDetailsSaveProperties = (Button) findViewById(R.id.btnWrkDetailsSaveProperties);
    }

    private void iniitComponentsListener() {
        mbtnWrkDetailsSaveProperties.setOnClickListener(OnClick);
    }

    View.OnClickListener OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.btnWrkDetailsSaveProperties:
                    // check if it is new worker or not
                    if (mWorker == null) {
                        mWorker = new Worker();
                    }
                    // get written details
                    String firstName = metWrkDetailsFirstName.getText().toString();
                    String lastName = metWrkDetailsLastName.getText().toString();
                    String israeliID = metWrkDetailsIsraeliID.getText().toString();
                    String tmp = metWrkDetailsDOB.getText().toString();

                    if (firstName.isEmpty() || lastName.isEmpty() || lastName.isEmpty() || tmp.isEmpty()) {
                        // check if first name field is empty
                        Toast errorToast = Toast.makeText(ActWorkerDetails.this, " First name field is empty! ", Toast.LENGTH_SHORT);
                        errorToast.show();
                    } else {
                        // check if israeliID number is correct
                        int numberId = Integer.parseInt(israeliID);
                        if (!CheckId(numberId)) {
                            Toast errorToast = Toast.makeText(ActWorkerDetails.this, " IsraeliID is not correct! ", Toast.LENGTH_SHORT);
                            errorToast.show();
                        }
                        int existingWorkerID;
                        //     get id from db if exist
                        existingWorkerID = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerIfIsraeliIDExist(israeliID);
                        //     check if israeliID is Exist
                        if (existingWorkerID != -1) {
                            Toast errorToast = Toast.makeText(ActWorkerDetails.this, " IsraeliID is Exist! ", Toast.LENGTH_SHORT);
                            errorToast.show();
                        }
                        // compare string to date
                        Date dateOfBirth = Utils.StringToDate(tmp);
                        // get user phone number
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
                        String phoneNumber = pref.getString(PhoneNumber, ""); // getting String

                        // save details
                        mWorker.setFirstName(firstName);
                        mWorker.setLastName(lastName);
                        mWorker.setIsraeliID(israeliID);
                        mWorker.setDob(dateOfBirth);
                        mWorker.setPhoneNumber(phoneNumber);

                        AppWorkerPresence.APP_INSTANCE.getWMDBAPI().saveWorker(mWorker);

                        // show message mWorker saved
                        Toast errorToast = Toast.makeText(ActWorkerDetails.this, " Worker saved successfully! ", Toast.LENGTH_SHORT);
                        errorToast.show();
                    }
            }
        }
    };

    // load worker details
    public void loadWorkerDetails(String aWorkerID) {

        mWorker = AppWorkerPresence.APP_INSTANCE.getArrayListWorkers().findWorkerById(aWorkerID);
        metWrkDetailsFirstName.setText(mWorker.getFirstName());
        metWrkDetailsLastName.setText(mWorker.getLastName());
        metWrkDetailsIsraeliID.setText(mWorker.getIsraeliID());
        String dateOfBirth = Utils.DateToString(mWorker.getDob());
        metWrkDetailsDOB.setText(dateOfBirth);

        mbtnWrkDetailsSaveProperties.setVisibility(View.INVISIBLE);
    }
    // check number of digits
    public static int SumDigits(int Number) {
        int sum = 0;
        while (Number != 0) {
            int temp1 = Number % 10;
            sum += temp1;
            Number = Number / 10;
        }
        return sum;
    }
    // check israel id by logic

    public static boolean CheckId(int number) {
        int LastNumber = number % 10;
        number = number / 10;
        int sum = 0;
        for (int i = 8; i >= 1; i--) {
            int calculate = number % 10;
            if (i % 2 == 1) {
                sum += calculate;
            } else {
                calculate = calculate * 2;
                int SumDigits = SumDigits(calculate);
                sum += SumDigits;
            }
            number = number / 10;
        }
        int res;
        if (sum % 10 == 0) {
            res = 0;
        } else {
            res = 10 - (Math.abs((sum % 10)));
        }

        if (LastNumber == res) {
            return true;
        } else {
            return false;
        }
    }

}