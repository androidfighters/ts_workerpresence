package il.co.techschool.workermanagement.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.Date;

import il.co.techschool.workermanagement.common.AppWorkerPresence;

/**
 * Created by Admin on 10/15/2017.
 */


public class Worker {
    @SerializedName("db_id")
    @Expose
    private String dbid;
    @SerializedName("first_name")
    @Expose
    private String firstname;
    @SerializedName("last_name")
    @Expose
    private String lastname;
    @SerializedName("israeliID")
    @Expose
    private String israeliID;
    @SerializedName("date_of_birth")
    @Expose
    private Date dateOfBirth;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;

    private Context mContext;

    public Worker() {
    }

    public Worker(String aFirstName, String aLastName, String aIsraeliID, Date aDob, String aDbID, String aPhoneNumber) {
        dbid = aDbID;
        firstname = aFirstName;
        lastname = aLastName;
        israeliID = aIsraeliID;
        dateOfBirth = aDob;
        phoneNumber = aPhoneNumber;
    }


    public String getDbId() {
        return dbid;
    }

    public void setDbId(String aDbId) {
        this.dbid = aDbId;
    }

    public String getFirstName() {
        return firstname;
    }

    public void setFirstName(String aFirstName) {

        if (aFirstName.length() < 2) {
            throw new RuntimeException("First name min length is 2 chars");
        }

        if (aFirstName.length() > 20) {
            throw new RuntimeException("First name max length is 20 chars");
        }

        this.firstname = aFirstName;
    }

    public String getLastName() {
        return lastname;
    }

    public void setLastName(String aLastName) {
        if (aLastName.length() < 2) {
            throw new RuntimeException("First name min length is 2 chars");
        }

        if (aLastName.length() > 20) {
            throw new RuntimeException("First name max length is 20 chars");
        }

        this.lastname = aLastName;
    }

    public String getIsraeliID() {
        return israeliID;
    }

    public void setIsraeliID(String aIsraeliID) {
        // isralei id check length
//        if (aIsraeliID.length() != 9) {
//            throw new RuntimeException("Israeli id must have 9 digits");
//        }
//
//        // Israileid check if it is numbers
//        if (!aIsraeliID.matches("[0-9]+")) {
//            throw new RuntimeException("Israeli id can have only digits");
//        }
//
//        // check israeli id by logic
//        int numberId = Integer.parseInt(aIsraeliID);
//        if (!CheckId(numberId)) {
//            throw new RuntimeException("Israeli id is not correct");
//        }
//
//        //  israeli id check if exist
//        int existingWorkerID;
//
//
//        existingWorkerID = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerIfIsraeliIDExist(aIsraeliID);
//
//        if (existingWorkerID != -1) {
//            throw new RuntimeException("Israeli id is Exist");
//        }

        this.israeliID = aIsraeliID;
    }

    public void setDob(Date aDob) {
        Calendar c = Calendar.getInstance();
        c.setTime(aDob);

        c.add(Calendar.YEAR, 18);
        Date dateOf18BirthDay = c.getTime();

        Date today = new Date();

        if (dateOf18BirthDay.after(today)) {
            throw new RuntimeException("Employee Age must be at least 18");
        }

        this.dateOfBirth = aDob;

    }

    public Date getDob() {
        return dateOfBirth;

    }

    public void setPhoneNumber(String aPhoneNumber) {
        this.phoneNumber = aPhoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    // check number of digits
    public static int SumDigits(int Number) {
        int sum = 0;
        while (Number != 0) {
            int temp1 = Number % 10;
            sum += temp1;
            Number = Number / 10;

        }

        return sum;
    }
// check israel id by logic

    public static boolean CheckId(int number) {

        int LastNumber = number % 10;
        number = number / 10;
        int sum = 0;
        for (int i = 8; i >= 1; i--) {
            int calculate = number % 10;
            if (i % 2 == 1) {
                sum += calculate;
            } else {
                calculate = calculate * 2;
                int SumDigits = SumDigits(calculate);
                sum += SumDigits;
            }
            number = number / 10;

        }
        int res;
        if (sum % 10 == 0) {
            res = 0;
        } else {
            res = 10 - (Math.abs((sum % 10)));
        }

        if (LastNumber == res) {
            return true;
        } else {
            return false;
        }
    }

    public void updateFrom(Worker aWorker) {
        firstname = aWorker.firstname;
        lastname = aWorker.lastname;
        israeliID = aWorker.israeliID;
        dateOfBirth = aWorker.dateOfBirth;
        dbid = aWorker.dbid;
        phoneNumber = aWorker.phoneNumber;

    }
}

