package il.co.techschool.workermanagement.activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;

import il.co.techschool.workermanagement.R;
import il.co.techschool.workermanagement.adapters.AdapterCompanies;
import il.co.techschool.workermanagement.adapters.AdapterProjects;
import il.co.techschool.workermanagement.common.AppWorkerPresence;

import static il.co.techschool.workermanagement.activites.ActLogin.PhoneNumber;

/**
 * Created by Admin on 10/23/2017.
 */

public class ActMain extends AppCompatActivity {
    private Button mbtnMainMyDetails;
    private Button mbtnMainSignOut;
    private Button mbtnMainCompanies;
    private Button mbtnMainProjects;
    private Button mbtnMainWorkTime;

    private ListView mlstMainProjects;
    private ListView mlstMainCompanies;

    private FirebaseAuth mAuth;

    private Context mContext;
    private AppWorkerPresence mAppWorkerPresence;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);

        mAuth = FirebaseAuth.getInstance();

        mAppWorkerPresence = (AppWorkerPresence) getApplication();
        mContext = this;

        initComponents();
        initComponentsListeners();

    }

    @Override
    protected void onResume() {
        super.onResume();

        // load my projects and my companies in a list
        AdapterProjects adapterProjects;
        AdapterCompanies adapterCompanies;

        adapterProjects = new AdapterProjects(mContext, AppWorkerPresence.APP_INSTANCE.getArrayListProjects());
        adapterCompanies = new AdapterCompanies(mContext, AppWorkerPresence.APP_INSTANCE.getArrayListCompanies());

        mlstMainProjects.setAdapter(adapterProjects);
        mlstMainCompanies.setAdapter(adapterCompanies);

    }

    private void initComponents() {
        mbtnMainSignOut = (Button) findViewById(R.id.btnMainSignOut);
        mbtnMainMyDetails = (Button) findViewById(R.id.btnMainMyDetails);
        mbtnMainCompanies = (Button) findViewById(R.id.btnMainCompanies);
        mbtnMainProjects = (Button) findViewById(R.id.btnMainProjects);
        mbtnMainWorkTime = (Button) findViewById(R.id.btnMainWorkTime);
        mlstMainProjects = (ListView) findViewById(R.id.lstMainProjects);
        mlstMainCompanies = (ListView) findViewById(R.id.lstMainCompanies);
    }

    private void initComponentsListeners() {
        mbtnMainSignOut.setOnClickListener(OnClick);
        mbtnMainMyDetails.setOnClickListener(OnClick);
        mbtnMainCompanies.setOnClickListener(OnClick);
        mbtnMainProjects.setOnClickListener(OnClick);
        mbtnMainWorkTime.setOnClickListener(OnClick);

    }

    private void signOut() {
        mAuth.signOut();
    }

    private View.OnClickListener OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnMainSignOut:
                    // logout and back to login activity
                    signOut();

                    Intent logout = new Intent(ActMain.this, ActLogin.class);
                    startActivity(logout);
                    break;

                case R.id.btnMainMyDetails:

                    // get the user phone number
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
                    String WorkerPhone = pref.getString(PhoneNumber, ""); // getting String
                    // get the user id by his phone number if he did save her details
                    String WorkerID = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerID(WorkerPhone);

                    // load worker details
                    Bundle dataBundle = new Bundle();
                    dataBundle.putString("ID", WorkerID);
                    Intent intent = new Intent(mContext, ActWorkerDetails.class);
                    intent.putExtras(dataBundle);
                    mContext.startActivity(intent);

                    break;

                case R.id.btnMainCompanies:
                    // create new company
                    Intent ActCompany = new Intent(ActMain.this, ActCompany.class);
                    startActivity(ActCompany);
                    break;

                case R.id.btnMainProjects:
                    // create new project
                    Intent ActProjects = new Intent(ActMain.this, ActProject.class);
                    startActivity(ActProjects);
                    break;

                case R.id.btnMainWorkTime:
                    // go to work time to start or finish work
                    Intent ActWorkTime = new Intent(ActMain.this, ActWorkTime.class);
                    startActivity(ActWorkTime);

                    break;

            }
        }
    };

}

