package il.co.techschool.workermanagement.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import il.co.techschool.workermanagement.R;
import il.co.techschool.workermanagement.activites.ActProject;
import il.co.techschool.workermanagement.activites.ActWorkerDetails;
import il.co.techschool.workermanagement.arrays.ArrayListWorkers;
import il.co.techschool.workermanagement.common.AppWorkerPresence;
import il.co.techschool.workermanagement.entities.Worker;

import static il.co.techschool.workermanagement.R.id.action_work_time_list;

/**
 * Created by Admin on 10/15/2017.
 */

public class AdapterWorkers extends BaseAdapter {

    private TextView mtxtWorkerName;
    private TextView mtxtWorkerID;
    private ImageButton mimgBtnWorker;
    private Context mContext;
    private ArrayListWorkers mArrayListWorkers;


    public AdapterWorkers(Context aContext, ArrayListWorkers aArrayListWorkers) {
        mArrayListWorkers = aArrayListWorkers;
        mContext = aContext;
    }

    @Override
    public int getCount() {
        return mArrayListWorkers.size();
    }

    @Override
    public Object getItem(int i) {
        return mArrayListWorkers.getLoaded();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View resultView;
        LayoutInflater layoutInflater;
        final Worker currentWorker;

        TextView txtWorkerName;
        TextView txtWorkerID;
        ImageButton imgBtnWorker;

        if (convertView == null) {
            layoutInflater = (LayoutInflater) AppWorkerPresence.APP_INSTANCE.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // create the custom layout to show the "workers"
            resultView = layoutInflater.inflate(R.layout.lo_item_worker, null);


            // find the objects inside the custom layout...
            txtWorkerName = (TextView) resultView.findViewById((R.id.txtItmWrkName));
            txtWorkerID = (TextView) resultView.findViewById((R.id.txtItmWrkID));
            imgBtnWorker = (ImageButton) resultView.findViewById((R.id.imgBtnItmWorker));

            // add all the "objects" to the "tag" list
            //resultView.setTag( R.id.<item it>, itemObject);

            resultView.setTag(R.id.txtItmWrkName, txtWorkerName);
            resultView.setTag(R.id.txtItmWrkID, txtWorkerID);
            resultView.setTag(R.id.imgBtnItmWorker, imgBtnWorker);


        } else {
            resultView = convertView;

            // extract all the pointers to the objects from the "tag" list.
            txtWorkerName = (TextView) resultView.getTag(R.id.txtItmWrkName);
            txtWorkerID = (TextView) resultView.getTag(R.id.txtItmWrkID);
            imgBtnWorker = (ImageButton) resultView.getTag(R.id.imgBtnItmWorker);
        }

        currentWorker = mArrayListWorkers.get(position);

        // update custom items in the view with the data from the "currentWorker";
        txtWorkerName.setText(currentWorker.getFirstName());
//        txtWorkerID.setText(currentWorker.getDbId());
        txtWorkerName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(mContext, " get click", Toast.LENGTH_SHORT).show();

                Bundle dataBundle = new Bundle();
                dataBundle.putString("id", currentWorker.getDbId());
                Intent intent = new Intent(mContext, ActWorkerDetails.class);
                intent.putExtras(dataBundle);
                mContext.startActivity(intent);

            }
        });
        try {
            imgBtnWorker.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    switch (v.getId()) {
                        case R.id.imgBtnItmWorker:

                            PopupMenu popup = new PopupMenu(mContext, v);
                            popup.getMenuInflater().inflate(R.menu.popup_menu_act_workers_list,
                                    popup.getMenu());
                            popup.show();
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {

                                        case R.id.action_show_on_map:

                                            //Or Some other code you want to put here.. This is just an example.
                                            //  Toast.makeText(mContext, " Install Clicked at position " + " : " + position, Toast.LENGTH_LONG).show();

                                            Intent chooser;
                                            Intent i = new Intent(Intent.ACTION_VIEW);
                                            String map = ("" + i);
                                            i.setData(Uri.parse("geo:" + i));
                                            chooser = Intent.createChooser(i, "launch maps");

                                            mContext.startActivity(chooser);
                                            break;
                                        case action_work_time_list:

                                            return true;

                                        case R.id.action_edit:
                                            Bundle dataBundle = new Bundle();
                                            dataBundle.putString("id", currentWorker.getDbId());
                                            Intent intent = new Intent(mContext, ActWorkerDetails.class);
                                            intent.putExtras(dataBundle);
                                            mContext.startActivity(intent);

                                            break;
                                        case R.id.action_delete:

                                            mtxtWorkerName.setVisibility(View.GONE);
                                            mtxtWorkerID.setVisibility(View.GONE);
                                            mimgBtnWorker.setVisibility(View.GONE);

                                            break;
                                        default:
                                            break;
                                    }

                                    return true;
                                }
                            });

                            break;

                        default:
                            break;
                    }

                }
            });

        } catch (Exception e) {

            e.printStackTrace();
        }
        return resultView;
    }
}