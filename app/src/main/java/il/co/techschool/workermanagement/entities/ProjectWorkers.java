package il.co.techschool.workermanagement.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 12/2/2017.
 */

public class ProjectWorkers {
    @SerializedName("db_id")
    @Expose
    private String dbId;
    @SerializedName("project_id")
    @Expose
    private Integer projectID;
    @SerializedName("worker_id")
    @Expose
    private Integer workerID;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("salary")
    @Expose
    private Integer salary;

    private Context mContext;

    public ProjectWorkers() {

    }

    public ProjectWorkers(String aDbId, Integer aProjectID, Integer aWorkerID, Integer aStatus, Integer aSalary) {

        dbId = aDbId;
        projectID = aProjectID;
        workerID = aWorkerID;
        status = aStatus;
        salary = aSalary;
    }

    public String getDbId() {
        return dbId;
    }

    public void setDbId(String aDbID) {
        dbId = aDbID;
    }

    public Integer getProjectID() {
        return projectID;
    }

    public void setProjectID(Integer aProjectID) {
        projectID = aProjectID;
    }

    public Integer getWorkerID() {
        return workerID;
    }

    public void setWorkerID(Integer aWorkerID) {
        workerID = aWorkerID;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer aStatus) {
        status = aStatus;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer aSalary) {
        salary = aSalary;
    }

    public void updateFrom(ProjectWorkers aProjectWorkers) {
        dbId = aProjectWorkers.dbId;
        projectID = aProjectWorkers.projectID;
        workerID = aProjectWorkers.workerID;
        status = aProjectWorkers.status;
        salary = aProjectWorkers.salary;
    }
}
