package il.co.techschool.workermanagement.activites;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import il.co.techschool.workermanagement.R;
import il.co.techschool.workermanagement.common.AppWorkerPresence;
import il.co.techschool.workermanagement.entities.Company;

/**
 * Created by Admin on 12/6/2017.
 */

public class ActCompany extends AppCompatActivity {
    private TextView mtxtCompanyIcon;
    private TextView mtxtCompanyTitle;
    private TextView mtxtCompanyName;
    private TextView mtxtCompanyLocation;
    private TextView mtxtCompanyOwnerPhone;

    private EditText metCompanyName;
    private EditText metCompanyLocation;
    private EditText metCompanyOwnerPhone;

    private Button mbtnCompanyWorkersList;
    private Button mbtnCompanyProjectsList;
    private Button mbtnCompanySave;
    private Button mbtnCompanyIcon;

    private ImageView mimgCompanyIcon;

    private static final int PICK_IMAGE = 100;
    final int REQUEST_CODE_GALLERY = 999;

    Uri mImgUri;

    private Context mContext;

    public Bundle getBundle = null;

    private Company mCompany;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_company);

        mContext = this;
        initComponents();
        initComponentsListeners();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getBundle = this.getIntent().getExtras();
        // check if there is a company to load its details or start new one
        if (getBundle != null) {
            if (getBundle.containsKey("id")) {
                loadCompanyDetails(getBundle.getString("id"));

            }
        }
    }

    void initComponents() {
        mtxtCompanyTitle = (TextView) findViewById(R.id.txtCompanyTitle);
        mtxtCompanyIcon = (TextView) findViewById(R.id.txtCompanyIcon);
        mtxtCompanyName = (TextView) findViewById(R.id.txtCompanyName);
        mtxtCompanyLocation = (TextView) findViewById(R.id.txtCompanyLocation);
        mtxtCompanyOwnerPhone = (TextView) findViewById(R.id.txtCompanyOwnerPhone);

        metCompanyName = (EditText) findViewById(R.id.etCompanyName);
        metCompanyLocation = (EditText) findViewById(R.id.etCompanyLocation);
        metCompanyOwnerPhone = (EditText) findViewById(R.id.etCompanyOwnerPhone);

        mbtnCompanyWorkersList = (Button) findViewById(R.id.btnCompanyWorkersList);
        mbtnCompanyProjectsList = (Button) findViewById(R.id.btnCompanyProjectsList);
        mbtnCompanySave = (Button) findViewById(R.id.btnCompanySave);
        mbtnCompanyIcon = (Button) findViewById(R.id.btnCompanyIcon);

        mimgCompanyIcon = (ImageView) findViewById(R.id.imgCompanyIcon);

    }

    void initComponentsListeners() {
        mbtnCompanyWorkersList.setOnClickListener(OnClick);
        mbtnCompanyProjectsList.setOnClickListener(OnClick);
        mbtnCompanySave.setOnClickListener(OnClick);
        mbtnCompanyIcon.setOnClickListener(OnClick);
    }

    View.OnClickListener OnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId()) {
                case R.id.btnCompanyWorkersList:
                    if (mCompany != null) {
                        // get CompanyID
                        String CompanyID = String.valueOf(mCompany.getDbId());
                        // set CompanyID
                        AppWorkerPresence.APP_INSTANCE.setCompanyID(CompanyID);
                        // load project workers in a list
                        Intent ActWorkersList = new Intent(ActCompany.this, ActCompanyWorkersList.class);
                        startActivity(ActWorkersList);

                    } else {
                        // show message
                        Toast errorToast = Toast.makeText(ActCompany.this, " There is no workers for this project yet ! ", Toast.LENGTH_SHORT);
                        errorToast.show();
                    }
                    break;
                case R.id.btnCompanyProjectsList:
                    if (mCompany != null) {
                        // get CompanyID
                        String CompanyID = String.valueOf(mCompany.getDbId());
                        // set CompanyID
                        AppWorkerPresence.APP_INSTANCE.setCompanyID(CompanyID);
                        // load project workers in a list
                        Intent ActProjectsList = new Intent(ActCompany.this, ActProjectsList.class);
                        startActivity(ActProjectsList);

                    } else {
                        // show message
                        Toast errorToast = Toast.makeText(ActCompany.this, " There is no workers for this company yet ! ", Toast.LENGTH_SHORT);
                        errorToast.show();
                    }

                    break;
                case R.id.btnCompanySave:

                    // save company details
                    saveCompanyDetails();
                    break;
                case R.id.btnCompanyIcon:

                    ActivityCompat.requestPermissions(
                            ActCompany.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_CODE_GALLERY
                    );

                    break;
            }
        }
    };

    public static byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CODE_GALLERY){
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK ,
                        MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                intent.setType("image/*");
                // try cut image
//                intent.putExtra("crop", "true");
//                intent.putExtra("aspectX", 1);
//                intent.putExtra("aspectY", 1);
//                intent.putExtra("outputX", 200);
//                intent.putExtra("outputY", 200);
//                intent.putExtra("return-data", true);
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            }
            else {
                Toast.makeText(getApplicationContext(), "You don't have permission to access file location!", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null){
            mImgUri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(mImgUri);

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                mimgCompanyIcon.setImageBitmap(bitmap);
                //mimgProjectIcon.setImageURI(mImgUri);
                //mimgProjectIcon.setImageBitmap((Bitmap) data.getExtras().get("data"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    /*
    save or update company
     */
    public void saveCompanyDetails() {
        if (mCompany == null) {
            mCompany = new Company();
        }

        // get written details
        String companyName = metCompanyName.getText().toString();
        String tmplocationID = metCompanyLocation.getText().toString();
        String tmpOwnerphone = metCompanyOwnerPhone.getText().toString();

        // check fields
        if (companyName.isEmpty() || tmplocationID.isEmpty() || tmpOwnerphone.isEmpty()) {
            // show message
            Toast errorToast = Toast.makeText(ActCompany.this, " field is empty! ", Toast.LENGTH_SHORT);
            errorToast.show();
        } else {
            int locationID = Integer.parseInt(tmplocationID);
            //get owner id
            String tmpOwnerID = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerID(tmpOwnerphone);
            if (tmpOwnerID == null){
                Toast errorToast = Toast.makeText(ActCompany.this, " Phone number is not correct! ", Toast.LENGTH_SHORT);
                errorToast.show();
            }else
            {
                int OwnerID = Integer.parseInt(tmpOwnerID);
                // save details
                mCompany.setCompanyName(companyName);
                mCompany.setLocationId(locationID);
                mCompany.setOwnerId(OwnerID);
                mimgCompanyIcon.setImageResource(R.mipmap.ic_launcher);

                if (mimgCompanyIcon != null)
                {
                    mCompany.setCompanyIcon(imageViewToByte(mimgCompanyIcon));
                }
                AppWorkerPresence.APP_INSTANCE.getWMDBAPI().saveCompany(mCompany);

                // show message mCompany saved
                Toast errorToast = Toast.makeText(ActCompany.this, " Company saved successfully! ", Toast.LENGTH_SHORT);
                errorToast.show();

                // intent back to main activity
                Intent actWorkersList = new Intent(ActCompany.this, ActMain.class);
                startActivity(actWorkersList);
            }
        }
    }

    /*
    load company
     */
    public void loadCompanyDetails(String aCompanyID) {
        mCompany = AppWorkerPresence.APP_INSTANCE.getArrayListCompanies().findCompanyById(aCompanyID);
        metCompanyName.setText(mCompany.getCompanyName());
        metCompanyLocation.setText(String.valueOf(mCompany.getLocationId()));
        metCompanyLocation.setText(String.valueOf(mCompany.getOwnerId()));

    }

}
