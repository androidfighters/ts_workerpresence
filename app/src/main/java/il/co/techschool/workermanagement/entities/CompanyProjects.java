package il.co.techschool.workermanagement.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 12/11/2017.
 */

public class CompanyProjects {
    @SerializedName("db_id")
    @Expose
    private String dbId;
    @SerializedName("company_id")
    @Expose
    private Integer companyId;
    @SerializedName("project_Id")
    @Expose
    private Integer projectId;

    public CompanyProjects() {

    }

    public CompanyProjects(String aDbId, Integer aCompanyId, Integer aProjectId) {

        dbId = aDbId;
        companyId = aCompanyId;
        projectId = aProjectId;

    }

    public String getDbId() {
        return dbId;
    }

    public void setDbId(String aDbId) {
        dbId = aDbId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer aCompanyId) {
        companyId = aCompanyId;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer aProjectId) {
        projectId = aProjectId;
    }

    public void updateFrom(CompanyProjects aCompanyProjects) {
        dbId = aCompanyProjects.dbId;
        companyId = aCompanyProjects.companyId;
        projectId = aCompanyProjects.projectId;
    }
}
