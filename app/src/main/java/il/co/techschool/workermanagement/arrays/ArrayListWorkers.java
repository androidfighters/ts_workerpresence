package il.co.techschool.workermanagement.arrays;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Date;

import il.co.techschool.workermanagement.entities.Worker;
import il.co.techschool.workermanagement.events.EventArrayListWorkersChange;

/**
 * Created by Admin on 10/15/2017.
 */

public class ArrayListWorkers extends ArrayList<Worker> {
    private Date loaded;

    public Date getLoaded() {
        return loaded;
    }

    public void setLoaded(Date loaded) {
        this.loaded = loaded;
    }


    public Worker findWorkerById(String aUid) {
        Worker workerFound;
        int iIndex;
        int iSize;

        workerFound = null;

        iSize = size();

        for (iIndex = 0; iIndex < iSize; iIndex++) {


            if (aUid.compareToIgnoreCase(get(iIndex).getDbId()) == 0) {
                workerFound = get(iIndex);
                break;
            }

        }

        return workerFound;
    }

    public Worker updateWorker(Worker aWorker) {
        Worker currentWorker;

        currentWorker = findWorkerById(aWorker.getDbId());

        if (currentWorker != null) {
            currentWorker.updateFrom(aWorker);
            EventBus.getDefault().post(new EventArrayListWorkersChange());
            return currentWorker;
        } else {
            return null;
        }
    }

    public void removeWorker(Worker aWorker) {
        Worker currentWorker;

        currentWorker = findWorkerById(aWorker.getDbId());

        if (currentWorker != null) {
            remove(currentWorker);
            EventBus.getDefault().post(new EventArrayListWorkersChange());
        }
    }

    @Override
    public boolean add(Worker aWorker) {
        boolean result;
        result = super.add(aWorker);
        EventBus.getDefault().post(new EventArrayListWorkersChange());
        return result;
    }

}
