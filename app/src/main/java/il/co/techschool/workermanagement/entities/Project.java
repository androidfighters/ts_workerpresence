package il.co.techschool.workermanagement.entities;

import android.content.Context;
import android.content.Intent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Admin on 11/26/2017.
 */

public class Project {
    @SerializedName("db_id")
    @Expose
    private Integer dbId;
    @SerializedName("project_name")
    @Expose
    private String projectName;
    @SerializedName("manager_id")
    @Expose
    private Integer managerId;
    @SerializedName("location_id")
    @Expose
    private Integer locationId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("start_date")
    @Expose
    private Date startDate;
    @SerializedName("end_date")
    @Expose
    private Date endDate;
    @SerializedName("project_icon")
    @Expose
    private byte[] projectIcon;


    private Context mContext;

    public Project() {
    }


    public Project(Integer aDbId, String aProjectName, Integer aManagerId, Integer aLocationId
            , String aDescription, Integer aStatus, Date aStartDate, Date aEndDate, byte[] aProjectIcon) {
        dbId = aDbId;
        projectName = aProjectName;
        managerId = aManagerId;
        locationId = aLocationId;
        description = aDescription;
        status = aStatus;
        startDate = aStartDate;
        endDate = aEndDate;
        projectIcon = aProjectIcon;
    }

    public Integer getDbId() {
        return dbId;
    }

    public void setDbId(Integer aDbId) {
        dbId = aDbId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String aProjectName) {
        projectName = aProjectName;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer aManagerId) {
        managerId = aManagerId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer aLocationId) {
        locationId = aLocationId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String aDescription) {
        description = aDescription;
    }

    public Integer getProjectStatus() {
        return status;
    }

    public void setProjectStatus(Integer aStatus) {
        status = aStatus;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date aStartDate) {
        startDate = aStartDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date aEndDate) {
        endDate = aEndDate;
    }

    public byte[] getProjectIcon()
    {
        return projectIcon;
    }

    public void setProjectIcon(byte[] aProjectIcon)
    {
        this.projectIcon = aProjectIcon;
    }

    public void updateFrom(Project aProject) {
        dbId = aProject.dbId;
        projectName = aProject.projectName;
        managerId = aProject.managerId;
        locationId = aProject.locationId;
        description = aProject.description;
        status = aProject.status;
        startDate = aProject.startDate;
        endDate = aProject.endDate;
        projectIcon = aProject.projectIcon;
    }

}
