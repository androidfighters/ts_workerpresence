package il.co.techschool.workermanagement.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.Date;

import il.co.techschool.workermanagement.R;
import il.co.techschool.workermanagement.arrays.ArrayListWorkTimeList;
import il.co.techschool.workermanagement.common.AppWorkerPresence;
import il.co.techschool.workermanagement.common.Utils;
import il.co.techschool.workermanagement.entities.ProjectWorkers;
import il.co.techschool.workermanagement.entities.WorkTimeList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by Admin on 10/16/2017.
 */

public class AdapterWorkTimeList extends BaseAdapter
{

    private Context mContext;
    private ArrayListWorkTimeList mArrayListWorkTimeList;

    public AdapterWorkTimeList(Context aContext, ArrayListWorkTimeList aArrayListWorkTimeList)
    {
        mArrayListWorkTimeList = aArrayListWorkTimeList;
        mContext = aContext;
    }

    @Override
    public int getCount()
    {
        return mArrayListWorkTimeList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return mArrayListWorkTimeList.get(position);
    }

    @Override
    public long getItemId(int i)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View resultView;
        LayoutInflater layoutInflater;
        final WorkTimeList currentWorkTime;

        TextView txtWorkerDay;
        TextView txtWorkerEnterWork;
        TextView txtWorkerExitWork;
        TextView txtProjectName;
        TextView txtTotalHours;
        ImageButton imgBtnEditTime;

        if (convertView == null) {
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // create the custom layout to show the "workers"
            resultView = layoutInflater.inflate(R.layout.lo_item_work_time, null);

            // find the objects inside the custom layout...
            txtWorkerDay = (TextView) resultView.findViewById((R.id.txtItmWrkDay));
            txtWorkerEnterWork = (TextView) resultView.findViewById((R.id.txtItmWrkEntranceWork));
            txtWorkerExitWork = (TextView) resultView.findViewById((R.id.txtItmWrkExitWrk));
            txtProjectName = (TextView) resultView.findViewById((R.id.txtItmWrkProject));
            txtTotalHours = (TextView) resultView.findViewById((R.id.txtItmWrkTotalHours));
            imgBtnEditTime = (ImageButton) resultView.findViewById((R.id.imgBtnWrkTimeLst));

            // add all the "objects" to the "tag" list
            //resultView.setTag( R.id.<item it>, itemObject);
            resultView.setTag(R.id.txtItmWrkDay, txtWorkerDay);
            resultView.setTag(R.id.txtItmWrkEntranceWork, txtWorkerEnterWork);
            resultView.setTag(R.id.txtItmWrkExitWrk, txtWorkerExitWork);
            resultView.setTag(R.id.txtItmWrkProject, txtProjectName);
            resultView.setTag(R.id.txtItmWrkTotalHours, txtTotalHours);
            resultView.setTag(R.id.imgBtnWrkTimeLst, imgBtnEditTime);

        } else {
            resultView = convertView;

            // extract all the pointers to the objects from the "tag" list.
            txtWorkerDay = (TextView) resultView.getTag(R.id.txtItmWrkDay);
            txtWorkerEnterWork = (TextView) resultView.getTag(R.id.txtItmWrkEntranceWork);
            txtWorkerExitWork = (TextView) resultView.getTag(R.id.txtItmWrkExitWrk);
            txtProjectName = (TextView) resultView.getTag(R.id.txtItmWrkProject);
            txtTotalHours = (TextView) resultView.getTag(R.id.txtItmWrkTotalHours);
            imgBtnEditTime = (ImageButton) resultView.getTag(R.id.imgBtnWrkTimeLst);
        }

        currentWorkTime = mArrayListWorkTimeList.get(position);

        // update custom items in the view with the data from the "currentWorker";
        txtWorkerDay.setText(currentWorkTime.getEntranceWork().toString());
        txtWorkerEnterWork.setText(currentWorkTime.getEntranceWork().toString());
        txtWorkerExitWork.setText(currentWorkTime.getExitWork().toString());
        txtProjectName.setText(currentWorkTime.getExitWork().toString());
        txtTotalHours.setText(currentWorkTime.getExitWork().toString());

        try {
            imgBtnEditTime.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    switch (v.getId()) {
                        case R.id.imgBtnWrkTimeLst:

                            PopupMenu popup = new PopupMenu(mContext, v);
                            popup.getMenuInflater().inflate(R.menu.popup_menu_act_work_time_list,
                                    popup.getMenu());
                            popup.show();
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                            {
                                @Override
                                public boolean onMenuItemClick(MenuItem item)
                                {
                                    switch (item.getItemId()) {
                                        case R.id.action_edit:

                                            int workTimeID = Integer.parseInt(currentWorkTime.getDbId());
                                            editWorkTimeDialog(workTimeID);
                                            break;
                                        default:
                                            break;
                                    }
                                    return true;
                                }
                            });

                            break;
                        default:
                            break;
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultView;
    }

    private void editWorkTimeDialog(final int aWorkTime) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.act_work_time_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText metWrkTimeDialogStart = (EditText) dialogView.findViewById(R.id.etWrkTimeDialogStart);
        final EditText metWrkTimeDialogFinish = (EditText) dialogView.findViewById(R.id.etWrkTimeDialogFinish);

        dialogBuilder.setTitle("Custom dialog");
        dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {

                WorkTimeList workTimeList = new WorkTimeList();

                String enterWorkHour = metWrkTimeDialogStart.getText().toString();
                String exitWorkHour = metWrkTimeDialogFinish.getText().toString();

                    int workTimeID = aWorkTime;
                    Date startTime = Utils.StringToDateTime(enterWorkHour);
                    Date finishTime = Utils.StringToDateTime(exitWorkHour);

                    workTimeList.setEntranceWork(startTime);
                    workTimeList.setEntranceWork(finishTime);

                    AppWorkerPresence.APP_INSTANCE.getWMDBAPI().updateWorkTime(workTimeList);

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }
}