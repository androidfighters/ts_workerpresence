package il.co.techschool.workermanagement.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.util.Arrays;
import java.util.Date;

import il.co.techschool.workermanagement.arrays.ArrayListCompanies;
import il.co.techschool.workermanagement.arrays.ArrayListLocation;
import il.co.techschool.workermanagement.arrays.ArrayListProjects;
import il.co.techschool.workermanagement.arrays.ArrayListWorkTimeList;
import il.co.techschool.workermanagement.arrays.ArrayListWorkers;
import il.co.techschool.workermanagement.common.Utils;
import il.co.techschool.workermanagement.entities.Company;
import il.co.techschool.workermanagement.entities.CompanyWorkers;
import il.co.techschool.workermanagement.entities.Project;
import il.co.techschool.workermanagement.entities.ProjectWorkers;
import il.co.techschool.workermanagement.entities.WorkTimeList;
import il.co.techschool.workermanagement.entities.Worker;
import il.co.techschool.workermanagement.entities.WorkerLocation;

import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_COMPANY_ID;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_COMPANY_NAME;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_DESCRIPTION;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_DOB;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_END_DATE;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_ENTER_LOCATION_ID;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_ENTER_WORK_DATE;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_EXIT_LOCATION_ID;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_EXIT_WORK_DATE;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_IMAGE;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_LATITUDE;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_LOCATION_ID;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_LONGITUDE;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_MANAGER_ID;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_OWNER_ID;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_PHONE_NUMBER;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_PROJECT_ID;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_PROJECT_NAME;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_SALARY;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_START_DATE;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_STATUS;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_WORKER_ID;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_WRK_FIRST_NAME;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_WRK_ISRAELI_ID;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.FLD_WRK_LAST_NAME;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.TBL_COMPANIES;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.TBL_COMPANY_PROJECTS;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.TBL_COMPANY_WORKERS;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.TBL_PROJECTS;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.TBL_PROJECT_WORKERS;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.TBL_WORKERS;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.TBL_WORKER_DAYS;
import static il.co.techschool.workermanagement.db.WMSQLiteOpenHelper.TBL_LOCATIONS;
import static org.greenrobot.eventbus.EventBus.TAG;

/**
 * Created by Admin on 10/15/2017.
 */

public class WMDBAPI {

    private Context mContext;
    private WMSQLiteOpenHelper mWMSQLiteOpenHelper;
    private SQLiteDatabase mSQLiteDatabaseRW;
    private SQLiteDatabase mSQLiteDatabaseRO;

    public WMDBAPI(Context aContext) {
        mContext = aContext;

        mWMSQLiteOpenHelper = new WMSQLiteOpenHelper(mContext);

        mSQLiteDatabaseRO = mWMSQLiteOpenHelper.getReadableDatabase();
        mSQLiteDatabaseRW = mWMSQLiteOpenHelper.getWritableDatabase();
    }

    public boolean saveWorker(Worker aWorker) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(FLD_WRK_FIRST_NAME, aWorker.getFirstName());
        contentValues.put(FLD_WRK_LAST_NAME, aWorker.getLastName());

        String tmp = Utils.DateToString(aWorker.getDob());

        contentValues.put(FLD_DOB, tmp);
        contentValues.put(FLD_WRK_ISRAELI_ID, aWorker.getIsraeliID());
        contentValues.put(FLD_PHONE_NUMBER, aWorker.getPhoneNumber());

        long result = mSQLiteDatabaseRW.insert(TBL_WORKERS, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;

    }

    public boolean saveWorkTime(WorkTimeList aWorkTimeList) {
        ContentValues contentValues = new ContentValues();

        String enterWork = Utils.DateTimeToString(aWorkTimeList.getEntranceWork());
        String exitWork = Utils.DateTimeToString(aWorkTimeList.getExitWork());

        contentValues.put(FLD_WORKER_ID, aWorkTimeList.getWorkerID());
        contentValues.put(FLD_ENTER_WORK_DATE, enterWork);
        contentValues.put(FLD_EXIT_WORK_DATE, exitWork);
        contentValues.put(FLD_ENTER_LOCATION_ID, aWorkTimeList.getEnterLocationID());
        contentValues.put(FLD_EXIT_LOCATION_ID, aWorkTimeList.getExitLocationID());
        contentValues.put(FLD_PROJECT_ID, aWorkTimeList.getProjectID());
        contentValues.put(FLD_COMPANY_ID, aWorkTimeList.getCompanyID());

            long result = mSQLiteDatabaseRW.insert(TBL_WORKER_DAYS, null, contentValues);
            if (result == -1)
                return false;
            else
                return true;

    }
    public boolean updateWorkTime(WorkTimeList aWorkTimeList){
        ContentValues contentValues = new ContentValues();
        String enterWork = Utils.DateTimeToString(aWorkTimeList.getEntranceWork());
        String exitWork = Utils.DateTimeToString(aWorkTimeList.getExitWork());

        contentValues.put(FLD_ENTER_WORK_DATE, enterWork);
        contentValues.put(FLD_EXIT_WORK_DATE, exitWork);
        long result = mSQLiteDatabaseRW.update(TBL_WORKER_DAYS,
                contentValues, BaseColumns._ID + " = " + aWorkTimeList.getDbId(), null);
        if (result == -1)
            return false;
         else
            return true;

    }
    public boolean saveWorkerLocation(WorkerLocation aWorkerLocation) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FLD_WORKER_ID, aWorkerLocation.getWorkerID());
        contentValues.put(FLD_LONGITUDE, aWorkerLocation.getLongitude());
        contentValues.put(FLD_LATITUDE, aWorkerLocation.getLatitude());

        long result = mSQLiteDatabaseRW.insert(TBL_LOCATIONS, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public boolean saveProject(Project aProject) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(FLD_PROJECT_NAME, aProject.getProjectName());
        contentValues.put(FLD_MANAGER_ID, aProject.getManagerId());
        contentValues.put(FLD_LOCATION_ID, aProject.getLocationId());
        contentValues.put(FLD_DESCRIPTION, aProject.getDescription());
        contentValues.put(FLD_STATUS, aProject.getProjectStatus());
        contentValues.put(FLD_START_DATE, aProject.getStartDate().getTime());
        contentValues.put(FLD_END_DATE, aProject.getEndDate().getTime());
        contentValues.put(FLD_IMAGE, aProject.getProjectIcon());

        if (aProject.getDbId() == null) {
            long result = mSQLiteDatabaseRW.insert(TBL_PROJECTS, null, contentValues);
            if (result == -1) {
                return false;
            } else {
                return true;
            }
        } else {
            long result = mSQLiteDatabaseRW.update(TBL_PROJECTS,
                    contentValues, BaseColumns._ID + "= " + aProject.getDbId(), null);
            if (result == -1) {
                return false;
            } else {
                return true;
            }
        }
    }

    public boolean addWorkersToProject(ProjectWorkers aProjectWorkers) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FLD_PROJECT_ID, aProjectWorkers.getProjectID());
        contentValues.put(FLD_WORKER_ID, aProjectWorkers.getWorkerID());
        contentValues.put(FLD_STATUS, aProjectWorkers.getStatus());
        contentValues.put(FLD_SALARY, aProjectWorkers.getSalary());

        long result = mSQLiteDatabaseRW.insert(TBL_PROJECT_WORKERS, null, contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Integer removeWorkerFromProject(Integer aWorkerID, Integer aProjectID) {

        int result = mSQLiteDatabaseRW.delete(TBL_PROJECT_WORKERS
                , FLD_WORKER_ID + " = " + Arrays.toString(new String[aWorkerID]) + " AND " + FLD_PROJECT_ID +
                        " = " + Arrays.toString(new String[aProjectID]), null);
        return result;
    }

    public boolean saveCompany(Company aCompany) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(FLD_COMPANY_NAME, aCompany.getCompanyName());
        contentValues.put(FLD_LOCATION_ID, aCompany.getLocationId());
        contentValues.put(FLD_OWNER_ID, aCompany.getOwnerId());
        contentValues.put(FLD_IMAGE, aCompany.getCompanyIcon());

        if (aCompany.getDbId() == null) {
            long result = mSQLiteDatabaseRW.insert(TBL_COMPANIES, null, contentValues);
            if (result == -1) {
                return false;
            } else {
                return true;
            }
        } else {
            long result = mSQLiteDatabaseRW.update(TBL_COMPANIES,
                    contentValues, BaseColumns._ID + "= " + aCompany.getDbId(), null);
            if (result == -1) {
                return false;
            } else {
                return true;
            }
        }
    }

    public boolean addWorkerToCompany(CompanyWorkers aCompanyWorkers) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(FLD_COMPANY_ID, aCompanyWorkers.getCompanyId());
        contentValues.put(FLD_WORKER_ID, aCompanyWorkers.getWorkerId());
        contentValues.put(FLD_SALARY, aCompanyWorkers.getSalary());

        long result = mSQLiteDatabaseRW.insert(TBL_COMPANY_WORKERS, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public Integer removeWorkerFromCompany(Integer aWorkerID, Integer aCompanyID) {
        int result = mSQLiteDatabaseRW.delete(TBL_COMPANY_WORKERS
                , FLD_WORKER_ID + "= " + Arrays.toString(new String[aWorkerID])
                        + " AND " + FLD_COMPANY_ID + " = " + Arrays.toString(new String[aCompanyID]), null);

        // aProjectWorkers.getWorkerID()
        return result;
    }

    public int loadWorkerIfIsraeliIDExist(String IsraeliID) {

        String sqlSearchWorker = " select " + BaseColumns._ID + " from " + TBL_WORKERS + " where "
                + FLD_WRK_ISRAELI_ID + " =" + IsraeliID;
        Cursor res = mSQLiteDatabaseRO.rawQuery(sqlSearchWorker, new String[]{});

        if (res.getCount() == 0) {
            return -1;
        } else {
            return res.getInt(res.getColumnIndex(BaseColumns._ID));
        }
    }

    public String loadWorkerID(String aWorkerPhone) {

        String sqlSearchWorker = " select " + BaseColumns._ID + " from " + TBL_WORKERS + " where"
                + " " + FLD_PHONE_NUMBER + " =" + "\"" + aWorkerPhone + "\"";
        Cursor res = mSQLiteDatabaseRO.rawQuery(sqlSearchWorker, new String[]{});
        if (res.getCount() == 0) {
            return null;
        } else {
            res.moveToFirst();
            return res.getString(res.getColumnIndex(BaseColumns._ID));
        }
    }

    public ArrayListWorkers loadWorkers() {

        ArrayListWorkers arrayListWorkers;
        arrayListWorkers = new ArrayListWorkers();
        Cursor res = mSQLiteDatabaseRO.rawQuery("select * from " + TBL_WORKERS, null);
        res.moveToFirst();
        while (!res.isAfterLast()) {

            String t3 = res.getString(res.getColumnIndex(FLD_DOB));
            Date tmp = Utils.StringToDate(t3);
            Worker worker = new Worker(res.getString(res.getColumnIndex(FLD_WRK_FIRST_NAME)),
                    res.getString(res.getColumnIndex(FLD_WRK_LAST_NAME)),
                    res.getString(res.getColumnIndex(FLD_WRK_ISRAELI_ID)),
                    tmp,
                    res.getString(res.getColumnIndex(BaseColumns._ID)),
                    res.getString(res.getColumnIndex(FLD_PHONE_NUMBER)));
            arrayListWorkers.add(worker);
            res.moveToNext();
        }
        arrayListWorkers.setLoaded(new Date());
        return arrayListWorkers;
    }

    public ArrayListWorkTimeList loadWorkTime(String aWorkerID) {
        ArrayListWorkTimeList arrayListWorkTimeList;
        arrayListWorkTimeList = new ArrayListWorkTimeList();

        String query = "select * from " + TBL_WORKER_DAYS +
                " WHERE " + FLD_WORKER_ID + " = " + Integer.parseInt(aWorkerID);
        Cursor res = mSQLiteDatabaseRO.rawQuery(query, new String[]{});
        res.moveToFirst();
        while (!res.isAfterLast()) {

            String entWrk = res.getString(res.getColumnIndex(FLD_ENTER_WORK_DATE));
            String exitWrk = res.getString(res.getColumnIndex(FLD_EXIT_WORK_DATE));
            Date tmpIn = Utils.StringToDateTime(entWrk);
            Date tmpOut = Utils.StringToDateTime(exitWrk);

            WorkTimeList workTimeList = new WorkTimeList(res.getInt(res.getColumnIndex(FLD_WORKER_ID))
                    , tmpIn, tmpOut, res.getString(res.getColumnIndex(BaseColumns._ID)),
                    res.getInt(res.getColumnIndex(FLD_ENTER_LOCATION_ID)),
                    res.getInt(res.getColumnIndex(FLD_EXIT_LOCATION_ID)),
                    res.getInt(res.getColumnIndex(FLD_PROJECT_ID)),
                    res.getInt(res.getColumnIndex(FLD_COMPANY_ID)));

            // mWorkTimeList.setEntranceWork(Utils.StringtoDateTime(res.getString(res.getColumnIndex(FLD_ENTER_WORK_DATE))));
            //  mWorkTimeList.setExitWork(Utils.StringtoDateTime(res.getString(res.getColumnIndex(FLD_EXIT_WORK_DATE))));
            arrayListWorkTimeList.add(workTimeList);

            res.moveToNext();
        }
        arrayListWorkTimeList.setLoaded(new Date());
        return arrayListWorkTimeList;
    }
    // TODO: 11/25/2017 load worker location from the db..
    public ArrayListLocation loadWorkerLocation(Integer workerID) {

        ArrayListLocation arrayListLocation = new ArrayListLocation();
        String query = "select * from " + TBL_LOCATIONS + " where " +
                FLD_WORKER_ID + " = " + String.valueOf(workerID);

        Cursor res = mSQLiteDatabaseRO.rawQuery(query, new String[]{});
         res.moveToFirst();
        while (!res.isAfterLast()) {

            WorkerLocation workerLocation = new WorkerLocation(res.getString(res.getColumnIndex(BaseColumns._ID)),
                    res.getInt(res.getColumnIndex(FLD_WORKER_ID)),
                    res.getString(res.getColumnIndex(FLD_LONGITUDE)),
                    res.getString(res.getColumnIndex(FLD_LATITUDE)));

            arrayListLocation.add(workerLocation);
            res.moveToNext();
        }
        arrayListLocation.setLoaded(new Date());
        return arrayListLocation;
    }

//    public Integer loadProjectID(String aPhoneNumber)
//    {
//
//        String sqlSearchProject = "select " + FLD_PROJECT_ID + "from " +
//                TBL_PROJECT_WORKERS + "where" + FLD_WORKER_ID + "= " + aPhoneNumber;
//        Cursor res = mSQLiteDatabaseRO.rawQuery(sqlSearchProject, new String[]{});
//
//        if (res.getCount() == 0)
//        {
//            return -1;
//        } else
//        {
//            return res.getInt(res.getColumnIndex(FLD_PROJECT_ID));
//        }
//    }

    public ArrayListProjects loadUserProjects(String aWorkerID) {

        ArrayListProjects arrayListProjects;
        arrayListProjects = new ArrayListProjects();

        String joinQuery = "select * from " + TBL_PROJECTS + " INNER JOIN " + TBL_PROJECT_WORKERS +
                " ON " + TBL_PROJECTS + "." + BaseColumns._ID + " = " + TBL_PROJECT_WORKERS + "." + FLD_PROJECT_ID + " WHERE " +
                TBL_PROJECT_WORKERS + "." + FLD_WORKER_ID + " = " + String.valueOf(aWorkerID) + " OR " + FLD_MANAGER_ID + " = " + Integer.parseInt(aWorkerID);
        // String joinQuery = "select * from " + TBL_PROJECTS + " where " + FLD_MANAGER_ID + " = " + Integer.parseInt(aWorkerID);
        // Log.d(TAG, joinQuery);
        //db browser
        // select * from projects where manager_id = 1
        // select * from projects  INNER JOIN  project_workers on projects._id = project_workers.project_id
        // WHERE project_workers.worker_id = 1 ;
        Cursor res = mSQLiteDatabaseRO.rawQuery(joinQuery, new String[]{});
        res.moveToFirst();
        while (!res.isAfterLast()) {

            Project project = new Project((res.getInt(res.getColumnIndex(BaseColumns._ID))),
                    res.getString(res.getColumnIndex(FLD_PROJECT_NAME)),
                    res.getInt(res.getColumnIndex(FLD_MANAGER_ID)),
                    res.getInt(res.getColumnIndex(FLD_LOCATION_ID)),
                    res.getString(res.getColumnIndex(FLD_DESCRIPTION)),
                    res.getInt(res.getColumnIndex(FLD_STATUS)),
                    new Date(res.getInt(res.getColumnIndex(FLD_START_DATE))),
                    new Date(res.getInt(res.getColumnIndex(FLD_END_DATE))),
                    res.getBlob(res.getColumnIndex(FLD_IMAGE))
            );
            arrayListProjects.add(project);
            res.moveToNext();
        }
        arrayListProjects.setLoaded(new Date());
        res.close();
        return arrayListProjects;
    }

    public ArrayListWorkers loadProjectWorkers(String aProjectID) {

        ArrayListWorkers arrayListWorkers = new ArrayListWorkers();
        String joinQuery = "select * from " + TBL_WORKERS + " INNER JOIN " + TBL_PROJECT_WORKERS +
                " ON " + TBL_WORKERS + "." + BaseColumns._ID + " = " + TBL_PROJECT_WORKERS + "." + BaseColumns._ID + " WHERE " +
                TBL_PROJECT_WORKERS + "." + FLD_COMPANY_ID + " = " + String.valueOf(aProjectID);
        Cursor res = mSQLiteDatabaseRO.rawQuery(joinQuery, new String[]{});
        res.moveToFirst();
        while (!res.isAfterLast()) {

            String t3 = res.getString(res.getColumnIndex(FLD_DOB));
            Date tmp = Utils.StringToDate(t3);
            Worker worker = new Worker(res.getString(res.getColumnIndex(FLD_WRK_FIRST_NAME)),
                    res.getString(res.getColumnIndex(FLD_WRK_LAST_NAME)),
                    res.getString(res.getColumnIndex(FLD_WRK_ISRAELI_ID)),
                    tmp,
                    res.getString(res.getColumnIndex(BaseColumns._ID)),
                    res.getString(res.getColumnIndex(FLD_PHONE_NUMBER)));
            arrayListWorkers.add(worker);
            res.moveToNext();
        }
        arrayListWorkers.setLoaded(new Date());
        res.close();

        return arrayListWorkers;
    }

    public ArrayListCompanies loadUserCompanies(String aWorkerID) {

        ArrayListCompanies arrayListCompanies;
        arrayListCompanies = new ArrayListCompanies();
        String joinQuery = "select * from " + TBL_COMPANIES + " INNER JOIN " + TBL_COMPANY_WORKERS +
                " ON " + TBL_COMPANIES + "." + BaseColumns._ID + " = " + TBL_COMPANY_WORKERS + "." + FLD_COMPANY_ID + " WHERE " +
                TBL_COMPANY_WORKERS + "." + FLD_COMPANY_ID + " = " + String.valueOf(aWorkerID) + " OR " + FLD_OWNER_ID + " = " + Integer.parseInt(aWorkerID);
        Cursor res = mSQLiteDatabaseRO.rawQuery(joinQuery, new String[]{});

        //Cursor res = mSQLiteDatabaseRO.rawQuery("select * from " + TBL_COMPANIES, null);
        res.moveToFirst();
        while (!res.isAfterLast()) {

            Company company = new Company((res.getString(res.getColumnIndex(BaseColumns._ID))),
                    res.getString(res.getColumnIndex(FLD_COMPANY_NAME)),
                    res.getInt(res.getColumnIndex(FLD_OWNER_ID)),
                    res.getInt(res.getColumnIndex(FLD_LOCATION_ID)),
                    res.getBlob(res.getColumnIndex(FLD_IMAGE))
            );

            arrayListCompanies.add(company);
            res.moveToNext();
        }
        arrayListCompanies.setLoaded(new Date());
        return arrayListCompanies;
    }

    public ArrayListWorkers loadCompanyWorkers(String aCompanyID) {
        ArrayListWorkers arrayListWorkers = new ArrayListWorkers();

        String joinQuery = "select * from " + TBL_WORKERS + " INNER JOIN " + TBL_COMPANY_WORKERS +
                " ON " + TBL_WORKERS + "." + BaseColumns._ID + " = " + TBL_COMPANY_WORKERS + "." + BaseColumns._ID + " WHERE " +
                TBL_COMPANY_WORKERS + "." + FLD_COMPANY_ID + " = " + String.valueOf(aCompanyID);
        Cursor res = mSQLiteDatabaseRO.rawQuery(joinQuery, new String[]{});
        res.moveToFirst();
        while (!res.isAfterLast()) {
            String t3 = res.getString(res.getColumnIndex(FLD_DOB));
            Date tmp = Utils.StringToDate(t3);
            Worker worker = new Worker(res.getString(res.getColumnIndex(FLD_WRK_FIRST_NAME)),
                    res.getString(res.getColumnIndex(FLD_WRK_LAST_NAME)),
                    res.getString(res.getColumnIndex(FLD_WRK_ISRAELI_ID)),
                    tmp,
                    res.getString(res.getColumnIndex(BaseColumns._ID)),
                    res.getString(res.getColumnIndex(FLD_PHONE_NUMBER)));
            arrayListWorkers.add(worker);
            res.moveToNext();
        }
        arrayListWorkers.setLoaded(new Date());
        res.close();

        return arrayListWorkers;
    }

    public ArrayListProjects loadCompanyProjects(String aCompanyID) {
        ArrayListProjects arrayListProjects = new ArrayListProjects();
        String joinQuery = "select * from " + TBL_PROJECTS + " INNER JOIN " + TBL_COMPANY_PROJECTS +
                " ON " + TBL_PROJECTS + "." + BaseColumns._ID + " = " + TBL_COMPANY_PROJECTS + "." + BaseColumns._ID + " WHERE " +
                TBL_COMPANY_PROJECTS + "." + FLD_COMPANY_ID + " = " + String.valueOf(aCompanyID);
        Cursor res = mSQLiteDatabaseRO.rawQuery(joinQuery, new String[]{});
        res.moveToFirst();
        while (!res.isAfterLast()) {

            Project project = new Project((res.getInt(res.getColumnIndex(BaseColumns._ID))),
                    res.getString(res.getColumnIndex(FLD_PROJECT_NAME)),
                    res.getInt(res.getColumnIndex(FLD_MANAGER_ID)),
                    res.getInt(res.getColumnIndex(FLD_LOCATION_ID)),
                    res.getString(res.getColumnIndex(FLD_DESCRIPTION)),
                    res.getInt(res.getColumnIndex(FLD_STATUS)),
                    new Date(res.getInt(res.getColumnIndex(FLD_START_DATE))),
                    new Date(res.getInt(res.getColumnIndex(FLD_END_DATE))),
                    res.getBlob(res.getColumnIndex(FLD_IMAGE))
            );
            arrayListProjects.add(project);
            res.moveToNext();
        }
        arrayListProjects.setLoaded(new Date());
        res.close();

        return arrayListProjects;
    }
}
//    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
//
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
//        return outputStream.toByteArray();
//    }
//
//    public Bitmap getImage(int i) {
//
//        String qu = "select img  from table where feedid=" + i;
//        Cursor cur = mSQLiteDatabaseRO.rawQuery(qu, null);
//
//        if (cur.moveToFirst()) {
//            byte[] imgByte = cur.getBlob(0);
//            cur.close();
//            return BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length);
//        }
//        if (cur != null && !cur.isClosed()) {
//            cur.close();
//        }
//        return null;
//    }