package il.co.techschool.workermanagement.activites;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import il.co.techschool.workermanagement.R;
import il.co.techschool.workermanagement.adapters.AdapterProjects;
import il.co.techschool.workermanagement.adapters.AdapterWorkers;
import il.co.techschool.workermanagement.common.AppWorkerPresence;

/**
 * Created by Admin on 12/6/2017.
 */

public class ActProjectsList extends AppCompatActivity {

    private ListView mlstLstProjects;

    private Context mContext;
    private AppWorkerPresence mAppWorkerPresence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_projects_list);

        mAppWorkerPresence = (AppWorkerPresence) getApplication();
        mContext = this;

        initComponents();

    }

    @Override
    protected void onResume() {
        super.onResume();

        AdapterProjects adapterProjects;
        // start using the adapter after you have the data loading...
        adapterProjects = new AdapterProjects(mContext, AppWorkerPresence.APP_INSTANCE.getArrayListProjects());

        mlstLstProjects.setAdapter(adapterProjects);

    }

    private void initComponents() {

        mlstLstProjects = (ListView) findViewById(R.id.lstLstProjects);
    }


}
