package il.co.techschool.workermanagement.arrays;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Date;

import il.co.techschool.workermanagement.entities.Project;
import il.co.techschool.workermanagement.events.EventArrayListProjectsChange;

/**
 * Created by Admin on 11/26/2017.
 */

public class ArrayListProjects extends ArrayList<Project> {
    private Date loaded;

    public Date getLoaded() {
        return loaded;
    }

    public void setLoaded(Date loaded) {
        this.loaded = loaded;
    }

    public Project findProjectById(Integer aUid) {
        Project projectFound;
        int iIndex;
        int iSize;

        projectFound = null;

        iSize = size();

        for (iIndex = 0; iIndex < iSize; iIndex++) {

            if (aUid.equals(get(iIndex).getDbId())) {
                projectFound = get(iIndex);
                break;
            }

        }

        return projectFound;
    }

    public Project updateProject(Project aProject) {
        Project currentProject;

        currentProject = findProjectById(aProject.getDbId());

        if (currentProject != null) {
            currentProject.updateFrom(aProject);
            EventBus.getDefault().post(new EventArrayListProjectsChange());
            return currentProject;
        } else {
            return null;
        }
    }

    public void removeProject(Project aProject) {
        Project currentProject;

        currentProject = findProjectById(aProject.getDbId());

        if (currentProject != null) {
            remove(currentProject);
            EventBus.getDefault().post(new EventArrayListProjectsChange());
        }
    }

    @Override
    public boolean add(Project aProject) {
        boolean result;
        result = super.add(aProject);
        EventBus.getDefault().post(new EventArrayListProjectsChange());
        return result;
    }
}
