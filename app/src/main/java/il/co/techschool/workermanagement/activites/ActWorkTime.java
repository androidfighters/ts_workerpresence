package il.co.techschool.workermanagement.activites;

import android.content.Context;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Calendar;
import java.util.Date;

import android.os.Handler;

import com.google.android.gms.maps.model.LatLng;

import il.co.techschool.workermanagement.R;
import il.co.techschool.workermanagement.common.AppWorkerPresence;
import il.co.techschool.workermanagement.common.Utils;
import il.co.techschool.workermanagement.entities.WorkTimeList;
import il.co.techschool.workermanagement.entities.Worker;
import il.co.techschool.workermanagement.entities.WorkerLocation;

/**
 * Created by Admin on 10/13/2017.
 */

public class ActWorkTime extends AppCompatActivity
{

    private WorkerLocation mWorkerlocation;
    private WorkTimeList mWorkTimeList;
    private Context mContext;
    private ToggleButton mtogbtnWrkTimeStartWork;
    private Button mbtnWrkTimeStartTimer;
    private Button mbtnWrkTimeStopTimer;
    private TextClock mtxtWrkTimeClock;
    private TextView mtxtWrkTimeMinutes;
    private EditText metWrkTimeValue;

    private CountDownTimer countDownTimer; // built in android class
    // CountDownTimer
    private long totalTimeCountInMilliseconds; // total count down time in
    // milliseconds
    private long timeBlinkInMilliseconds; // start time of start blinking
    private boolean blink; // controls the blinking .. on and off

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_work_time);

        initComponents();
        iniCompontListener();

    }

    private void initComponents()
    {
        mtxtWrkTimeClock = (TextClock) findViewById(R.id.txtWrkTimeClock);
        mtxtWrkTimeMinutes = (TextView) findViewById(R.id.txtWrkTimeMinutes);
        metWrkTimeValue = (EditText) findViewById(R.id.etWrkTimeValue);
        mtogbtnWrkTimeStartWork = (ToggleButton) findViewById(R.id.togbtnWrkTimeStartWork);
        mbtnWrkTimeStartTimer = (Button) findViewById(R.id.btnWrkTimeStartTimer);
        mbtnWrkTimeStopTimer = (Button) findViewById(R.id.btnWrkTimeStopTimer);

    }

    private void iniCompontListener()
    {
        mtogbtnWrkTimeStartWork.setOnCheckedChangeListener(onCheckedChanged);
        mbtnWrkTimeStartTimer.setOnClickListener(OnClick);
        mbtnWrkTimeStopTimer.setOnClickListener(OnClick);
    }

    CompoundButton.OnCheckedChangeListener onCheckedChanged = new CompoundButton.OnCheckedChangeListener()
    {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked)
        {

            mWorkTimeList = new WorkTimeList();
            mWorkerlocation = new WorkerLocation();
            Date currentTime = Calendar.getInstance().getTime();
            if (isChecked)
            {
                // start work
                // location on
                statusCheck();
                // mapsActivity.getWorkerLocation();
                AppWorkerPresence.APP_INSTANCE.getLatlng();

                // get user id
                String workerID = AppWorkerPresence.APP_INSTANCE.getWorkerID();
                // compare to integer
                int WorkerID = Integer.parseInt(workerID);

                mWorkerlocation.setWorkerID(WorkerID);
//                mWorkerlocation.setLatitude();
//                mWorkerlocation.setLongitude();

                // send start work time to the db
                AppWorkerPresence.APP_INSTANCE.getWMDBAPI().saveWorkerLocation(mWorkerlocation);

                AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerLocation(WorkerID);
                mWorkTimeList.setWorkerID(WorkerID);

                mWorkTimeList.setEntranceWork(currentTime);

                AppWorkerPresence.APP_INSTANCE.getWMDBAPI().saveWorkTime(mWorkTimeList);

            } else
            {

                // The toggle is disabled
                // finish work

                // location on
                statusCheck();
                // mapsActivity.getWorkerLocation();
                AppWorkerPresence.APP_INSTANCE.getLatlng();

                // get user id
                String workerID = AppWorkerPresence.APP_INSTANCE.getWorkerID();
                // compare to integer
                int WorkerID = Integer.parseInt(workerID);

                mWorkerlocation.setWorkerID(WorkerID);
//                mWorkerlocation.setLatitude();
//                mWorkerlocation.setLongitude();

                // send finish work location to the db
                AppWorkerPresence.APP_INSTANCE.getWMDBAPI().saveWorkerLocation(mWorkerlocation);

                mWorkTimeList.setWorkerID(WorkerID);

                mWorkTimeList.setExitWork(currentTime);

                AppWorkerPresence.APP_INSTANCE.getWMDBAPI().updateWorkTime(mWorkTimeList);

            }
        }

    };

    View.OnClickListener OnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.btnWrkTimeStartTimer:

                    mtxtWrkTimeMinutes.setTextAppearance(getApplicationContext(),
                            R.style.normalText);
                    setTimer();
                    mbtnWrkTimeStopTimer.setVisibility(View.VISIBLE);
                    mbtnWrkTimeStartTimer.setVisibility(View.GONE);
                    metWrkTimeValue.setVisibility(View.GONE);
                    metWrkTimeValue.setText("");
                    startTimer();
                    break;
                case R.id.btnWrkTimeStopTimer:

                    countDownTimer.cancel();
                    mbtnWrkTimeStartTimer.setVisibility(View.VISIBLE);
                    mbtnWrkTimeStopTimer.setVisibility(View.GONE);
                    metWrkTimeValue.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    private void setTimer()
    {
        int time = 0;
        if (!metWrkTimeValue.getText().toString().equals(""))
        {
            time = Integer.parseInt(metWrkTimeValue.getText().toString());
        } else
            Toast.makeText(ActWorkTime.this, "Please Enter Minutes...",
                    Toast.LENGTH_LONG).show();

        totalTimeCountInMilliseconds = 60 * time * 1000;

        timeBlinkInMilliseconds = 30 * 1000;
    }

    private void startTimer()
    {
        countDownTimer = new CountDownTimer(totalTimeCountInMilliseconds, 500)
        {
            // 500 means, onTick function will be called at every 500
            // milliseconds
            @Override
            public void onTick(long leftTimeInMilliseconds)
            {
                long seconds = leftTimeInMilliseconds / 1000;

                if (leftTimeInMilliseconds < timeBlinkInMilliseconds)
                {
                    mtxtWrkTimeMinutes.setTextAppearance(getApplicationContext(),
                            R.style.blinkText);
                    // change the style of the textview .. giving a red
                    // alert style

                    if (blink)
                    {
                        mtxtWrkTimeMinutes.setVisibility(View.VISIBLE);
                        // if blink is true, textview will be visible
                    } else
                    {
                        mtxtWrkTimeMinutes.setVisibility(View.INVISIBLE);
                    }

                    blink = !blink; // toggle the value of blink
                }

                mtxtWrkTimeMinutes.setText(String.format("%02d", seconds / 60)
                        + ":" + String.format("%02d", seconds % 60));
                // format the textview to show the easily readable format

            }

            @Override
            public void onFinish()
            {
                // this function will be called when the timecount is finished
                mtxtWrkTimeMinutes.setText("Time up!");
                mtxtWrkTimeMinutes.setVisibility(View.VISIBLE);
                mbtnWrkTimeStartTimer.setVisibility(View.VISIBLE);
                mbtnWrkTimeStopTimer.setVisibility(View.GONE);
                metWrkTimeValue.setVisibility(View.VISIBLE);
            }

        }.start();
    }

    // turn on Location
    public void statusCheck()
    {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            buildAlertMessageGps();
        }
    }

    //Show message
    private void buildAlertMessageGps()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS  be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    public void onClick(final DialogInterface dialog, final int id)
                    {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener()
                {
                    public void onClick(final DialogInterface dialog, final int id)
                    {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}