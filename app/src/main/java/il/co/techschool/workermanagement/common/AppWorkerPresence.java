package il.co.techschool.workermanagement.common;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.model.LatLng;

import il.co.techschool.workermanagement.activites.ActLogin;
import il.co.techschool.workermanagement.arrays.ArrayListCompanies;
import il.co.techschool.workermanagement.arrays.ArrayListLocation;
import il.co.techschool.workermanagement.arrays.ArrayListProjectWorkers;
import il.co.techschool.workermanagement.arrays.ArrayListProjects;
import il.co.techschool.workermanagement.arrays.ArrayListWorkTimeList;
import il.co.techschool.workermanagement.arrays.ArrayListWorkers;
import il.co.techschool.workermanagement.db.WMDBAPI;
import il.co.techschool.workermanagement.entities.Project;
import il.co.techschool.workermanagement.entities.Worker;
import il.co.techschool.workermanagement.firebase.WMFirebase;
import io.fabric.sdk.android.Fabric;

import static il.co.techschool.workermanagement.activites.ActLogin.PhoneNumber;

/**
 * Created by Admin on 10/15/2017.
 */

public class AppWorkerPresence extends Application {
    private WMDBAPI mWMDBAPI;

    public static AppWorkerPresence APP_INSTANCE = null;

    public String mProjectID;
    public String mWorkerID;
    public String mUserPhone;
    public String mCompanyID;
    private LatLng mLatlng;

    private ArrayListWorkers mArrayListWorkers;
    private ArrayListWorkTimeList mArrayListWorkTimeList;
    private ArrayListProjects mArrayListProjects;
    private ArrayListProjectWorkers mArrayListProjectWorkers;
    private ArrayListCompanies mArrayListCompanies;
    private ArrayListLocation mArrayListLocation;

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());

        mWMDBAPI = new WMDBAPI(this);

        mArrayListWorkers = new ArrayListWorkers();
        mArrayListWorkTimeList = new ArrayListWorkTimeList();
        mArrayListProjects = new ArrayListProjects();
        mArrayListProjectWorkers = new ArrayListProjectWorkers();
        mArrayListCompanies = new ArrayListCompanies();
        mArrayListLocation = new ArrayListLocation();


        SharedPreferences pref = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
        mUserPhone = pref.getString(PhoneNumber, ""); // getting String

        mWorkerID = mWMDBAPI.loadWorkerID(mUserPhone);


        if (mWorkerID != null) {
            mArrayListProjects = mWMDBAPI.loadUserProjects(mWorkerID);
            mArrayListCompanies = mWMDBAPI.loadUserCompanies(mWorkerID);
            mArrayListWorkTimeList = mWMDBAPI.loadWorkTime(mWorkerID);
            mArrayListLocation = mWMDBAPI.loadWorkerLocation(Integer.parseInt(mWorkerID));

            setWorkerID(mWorkerID);
        }
        if (mProjectID != null) {
            mArrayListWorkers = mWMDBAPI.loadProjectWorkers(getProjectID());

        }
        mArrayListWorkers = mWMDBAPI.loadWorkers();


        if (mCompanyID != null) {
            mArrayListWorkers = mWMDBAPI.loadCompanyWorkers(getCompanyID());
            mArrayListProjects = mWMDBAPI.loadCompanyProjects(getCompanyID());

        }
        APP_INSTANCE = this;

        registerActivityLifecycleCallbacks(new AppLifecycleTracker());

        WMFirebase.getInstance();
    }

    @Override
    public void onTerminate() {
        WMFirebase.getInstance().updateWorkersStatus(WMFirebase.getInstance().getCurrentUser().getUid(), 0);

        super.onTerminate();
    }


    public ArrayListWorkers getArrayListWorkers() {
        return mArrayListWorkers;
    }

    public ArrayListWorkTimeList getArrayListWorkTimeList() {
        return mArrayListWorkTimeList;
    }

    public ArrayListProjects getArrayListProjects() {
        return mArrayListProjects;
    }

    //    public ArrayListProjectWorkers getArrayListProjectWorkers(){
//        return mArrayListProjectWorkers;
//    }
    public ArrayListCompanies getArrayListCompanies() {
        return mArrayListCompanies;

    }

    public LatLng getLatlng (){
        return mLatlng;
    }
    public void setLatlng(LatLng aLatlng){
        mLatlng = aLatlng;
    }
    public String getWorkerID() {
        return mWorkerID;
    }

    public void setWorkerID(String aWorkerID) {
        mWorkerID = aWorkerID;
    }
    public String getProjectID() {
        return mProjectID;
    }

    public void setProjectID(String aProjectID) {
        mProjectID = aProjectID;
    }

    public String getCompanyID() {
        return mCompanyID;
    }

    public void setCompanyID(String aCompanyID) {
        mCompanyID = aCompanyID;
    }

    public Worker getCurrentWorker() {
        return mArrayListWorkers.findWorkerById(WMFirebase.getInstance().getCurrentUser().getUid());
    }


    public Project getCurrentProject() {
        return mArrayListProjects.findProjectById(Integer.parseInt(WMFirebase.getInstance().getCurrentUser().getUid()));
    }

    public WMDBAPI getWMDBAPI() {
        return mWMDBAPI;
    }

    private class AppLifecycleTracker implements ActivityLifecycleCallbacks {

        private int numStarted = 0;

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {
            numStarted++;
        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        // need to add status for worker job
        @Override
        public void onActivityStopped(Activity activity) {
            numStarted--;
            if (numStarted == 0) {
                //     WMFirebase.getInstance().updateWorkerStatus(WMFirebase.getInstance().getCurrentUser().getUid(), 0);
            }
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    }
}
