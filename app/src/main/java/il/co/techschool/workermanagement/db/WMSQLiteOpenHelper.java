package il.co.techschool.workermanagement.db;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * Created by Admin on 10/15/2017.
 */

public class WMSQLiteOpenHelper extends SQLiteOpenHelper {

    public static final String TAG = "WMSQLiteOpenHelper";

    public final static int DB_VERSION = 3;
    public final static String DB_NAME = "worker_management.db";

    // tables in the db
    public final static String TBL_WORKERS = "workers";
    public final static String TBL_WORKER_DAYS = "worker_days";
    public final static String TBL_LOCATIONS = "locations";

    public final static String TBL_PROJECTS = "projects";
    public final static String TBL_PROJECT_WORKERS = "project_workers";
    public final static String TBL_PROJECT_IMAGES = "project_images";

    public final static String TBL_COMPANIES = "companies";
    public final static String TBL_COMPANY_WORKERS = "company_workers";
    public final static String TBL_COMPANY_PROJECTS = "company_projects";
    public final static String TBL_COMPANY_IMAGES = "company_images";

    // table worker properties
    public final static String FLD_WRK_FIRST_NAME = "first_name";
    public final static String FLD_WRK_LAST_NAME = "last_name";
    public final static String FLD_WRK_ISRAELI_ID = "israeli_id";
    public final static String FLD_WORKER_ID = "worker_id";
    public static final String FLD_DOB = "date_of_birth";
    public static final String FLD_PHONE_NUMBER = "phone_number";

    // table project properties
    public final static String FLD_PROJECT_NAME = "project_name";
    public final static String FLD_MANAGER_ID = "manager_id";
    public final static String FLD_LOCATION_ID = "location_id";
    public final static String FLD_STATUS = "status";
    public final static String FLD_DESCRIPTION = "description";
    public final static String FLD_START_DATE = "start_date";
    public static final String FLD_END_DATE = "end_date";
    public final static String FLD_SALARY = "salary";
    public final static String FLD_IMAGE = "image";

    // table worker time
    public static final String FLD_ENTER_WORK_DATE = "date_enter";
    public static final String FLD_EXIT_WORK_DATE = "date_exit";
    public static final String FLD_ENTER_LOCATION_ID = "enter_location_id";
    public static final String FLD_EXIT_LOCATION_ID = "exit_location_id";


    // table location
    public static final String FLD_LONGITUDE = "longitude";
    public static final String FLD_LATITUDE = "latitude";

    // table project worker
    public final static String FLD_PROJECT_ID = "project_id";

    // table company
    public final static String FLD_COMPANY_NAME = "company_name";

    // table company worker
    public final static String FLD_COMPANY_ID = "company_id";

    // table company projects

    //General Field types
    public static final String FLD_BASE_TYPE_KEY = "integer primary key autoincrement";
    public static final String FLD_TYPE_INTEGER = "integer";
    public static final String FLD_TYPE_REAL = "real";
    public static final String FLD_TYPE_BLOB = "blob";
    public static final String FLD_TYPE_TIMESTAMP = "timestamp";
    public static final String FLD_TYPE_VARCHAR_10 = "nvarchar(10)";
    public static final String FLD_TYPE_VARCHAR_15 = "nvarchar(15)";
    public static final String FLD_TYPE_VARCHAR_20 = "nvarchar(20)";
    public static final String FLD_TYPE_VARCHAR_50 = "nvarchar(50)";
    // public static final String FLD_TYPE_BOOLEAN = "boolean";

    //  public static final String FLD_WORKED = "boolean";
    public static final String FLD_WORK_DATE = "work_date";
    public static final String FLD_OWNER_ID = "owner_id";


    public WMSQLiteOpenHelper(Context aContext) {
        this(aContext, DB_NAME, null, DB_VERSION);

    }

    public WMSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    private WMSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }


    private DatabaseErrorHandler mDatabaseErrorHandler = new DatabaseErrorHandler() {
        @Override
        public void onCorruption(SQLiteDatabase dbObj) {
            Log.d(TAG, "db corrupted");
        }
    };

    @Override
    public void onCreate(SQLiteDatabase db) {

        String strSQL;

        strSQL = "CREATE TABLE " + TBL_WORKERS + " " + " (" +
                BaseColumns._ID + " " + FLD_BASE_TYPE_KEY + " NOT NULL " + "," +
                FLD_WRK_FIRST_NAME + " " + FLD_TYPE_VARCHAR_20 + " NOT NULL " + "," +
                FLD_WRK_LAST_NAME + " " + FLD_TYPE_VARCHAR_20 + " NOT NULL " + "," +
                FLD_DOB + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_WRK_ISRAELI_ID + " " + FLD_TYPE_VARCHAR_10 + " NOT NULL " + "," +
                FLD_PHONE_NUMBER + " " + FLD_TYPE_VARCHAR_15 + " NOT NULL"
                + ");";

        Log.d(TAG, strSQL);
        db.execSQL(strSQL);

        strSQL = "CREATE TABLE " + TBL_WORKER_DAYS + " " + " (" +
                BaseColumns._ID + " " + FLD_BASE_TYPE_KEY + " NOT NULL " + "," +
                FLD_WORKER_ID + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_ENTER_WORK_DATE + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_EXIT_WORK_DATE + " " + FLD_TYPE_INTEGER + "," +
                FLD_ENTER_LOCATION_ID + " " + FLD_TYPE_INTEGER + " ," +
                FLD_EXIT_LOCATION_ID + " " + FLD_TYPE_INTEGER + " ," +
                FLD_PROJECT_ID + " " + FLD_TYPE_INTEGER + "," +
                FLD_COMPANY_ID + " " + FLD_TYPE_INTEGER
                + " );";

        Log.d(TAG, strSQL);
        db.execSQL(strSQL);

        strSQL = "CREATE TABLE " + TBL_LOCATIONS + " " + " (" +
                BaseColumns._ID + " " + FLD_BASE_TYPE_KEY + " NOT NULL " + "," +
                FLD_WORKER_ID + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_LONGITUDE + " " + FLD_TYPE_REAL + " NOT NULL " + "," +
                FLD_LATITUDE + " " + FLD_TYPE_REAL + " NOT NULL "
                + " );";

        Log.d(TAG, strSQL);
        db.execSQL(strSQL);
        strSQL = "CREATE TABLE " + TBL_PROJECTS + " " + " (" +
                BaseColumns._ID + " " + FLD_BASE_TYPE_KEY + " NOT NULL " + "," +
                FLD_PROJECT_NAME + " " + FLD_TYPE_VARCHAR_50 + " NOT NULL " + "," +
                FLD_MANAGER_ID + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_LOCATION_ID + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_DESCRIPTION + " " + FLD_TYPE_VARCHAR_50 + " NOT NULL " + "," +
                FLD_STATUS + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_START_DATE + " " + FLD_TYPE_INTEGER + " NOT NULL " + " ," +
                FLD_END_DATE + " " + FLD_TYPE_INTEGER + "," +
                FLD_IMAGE + " " + FLD_TYPE_BLOB
                + " );";
        Log.d(TAG, strSQL);
        db.execSQL(strSQL);

        strSQL = "CREATE TABLE " + TBL_PROJECT_WORKERS + " " + " (" +
                BaseColumns._ID + " " + FLD_BASE_TYPE_KEY + " NOT NULL" + "," +
                FLD_PROJECT_ID + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_WORKER_ID + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_STATUS + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_SALARY + " " + FLD_TYPE_INTEGER
                + " );";
        Log.d(TAG, strSQL);
        db.execSQL(strSQL);

        strSQL = "CREATE TABLE " + TBL_COMPANIES + " " + " (" +
                BaseColumns._ID + " " + FLD_BASE_TYPE_KEY + " NOT NULL" + "," +
                FLD_COMPANY_NAME + " " + FLD_TYPE_VARCHAR_50 + " NOT NULL " + "," +
                FLD_LOCATION_ID + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_OWNER_ID + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_IMAGE + " " + FLD_TYPE_BLOB
                + " );";
        Log.d(TAG, strSQL);
        db.execSQL(strSQL);

        strSQL = "CREATE TABLE " + TBL_COMPANY_WORKERS + " " + " (" +
                BaseColumns._ID + " " + FLD_BASE_TYPE_KEY + " NOT NULL" + "," +
                FLD_COMPANY_ID + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_WORKER_ID + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_STATUS + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_SALARY + " " + FLD_TYPE_INTEGER
                + " );";
        Log.d(TAG, strSQL);
        db.execSQL(strSQL);

        strSQL = "CREATE TABLE " + TBL_COMPANY_PROJECTS + " " + " (" +
                BaseColumns._ID + " " + FLD_BASE_TYPE_KEY + " NOT NULL" + "," +
                FLD_COMPANY_ID + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_PROJECT_ID + " " + FLD_TYPE_INTEGER + " NOT NULL " + "," +
                FLD_STATUS + " " + FLD_TYPE_INTEGER + " NOT NULL "
                + " );";
        Log.d(TAG, strSQL);
        db.execSQL(strSQL);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TBL_WORKERS + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TBL_WORKER_DAYS + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TBL_LOCATIONS + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TBL_PROJECTS + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TBL_PROJECT_WORKERS + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TBL_COMPANIES + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TBL_COMPANY_WORKERS + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TBL_COMPANY_PROJECTS + ";");

        onCreate(db);

    }
}
